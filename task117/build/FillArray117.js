"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FillArray117 = void 0;
class FillArray117 {
    constructor() {
    }
    fillArray() {
        const len = 9;
        const array = [];
        for (let i1 = 1; i1 <= len; i1++) {
            if (this.isEqual(i1))
                continue;
            for (let i2 = 0; i2 <= len; i2++) {
                if (this.isEqual(i2, i1))
                    continue;
                //------------------
                for (let i3 = 0; i3 <= len; i3++) {
                    if (this.isEqual(i3, i1, i2))
                        continue;
                    //------------------
                    for (let i4 = 0; i4 <= len; i4++) {
                        if (this.isEqual(i4, i1, i2, i3))
                            continue;
                        //------------------
                        for (let i5 = 0; i5 <= len; i5++) {
                            if (this.isEqual(i5, i1, i2, i3, i4))
                                continue;
                            //------------------
                            for (let i6 = 0; i6 <= len; i6++) {
                                if (this.isEqual(i6, i1, i2, i3, i4, i5))
                                    continue;
                                //------------------
                                for (let i7 = 0; i7 <= len; i7++) {
                                    if (this.isEqual(i7, i1, i2, i3, i4, i5, i6))
                                        continue;
                                    //------------------
                                    for (let i8 = 0; i8 <= len; i8++) {
                                        if (this.isEqual(i8, i1, i2, i3, i4, i5, i6, i7))
                                            continue;
                                        //------------------
                                        for (let i9 = 0; i9 <= len; i9++) {
                                            if (this.isEqual(i9, i1, i2, i3, i4, i5, i6, i7, i8))
                                                continue;
                                            //------------------
                                            for (let i10 = 0; i10 <= len; i10++) {
                                                if (this.isEqual(i10, i1, i2, i3, i4, i5, i6, i7, i8, i9))
                                                    continue;
                                                array.push([i1, i2, i3, i4, i5, i6, i7, i8, i9, i10]);
                                                // 	console.log(`${i1} ${i2} ${i3} ${i4} ${i5} ${i6} ${i7} ${i8} ${i9} ${i10}`)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return array;
    }
    isEqual(x1, ...x2) {
        return x2.some(item => item === x1);
    }
}
exports.FillArray117 = FillArray117;
// const fillArray117 = new FillArray117();
// console.log(
// 	fillArray117.fillArray()
// )
//# sourceMappingURL=FillArray117.js.map