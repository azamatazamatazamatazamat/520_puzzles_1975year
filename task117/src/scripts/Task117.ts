import {FillArray117} from "./FillArray117";

type Operation = (x: number)=>number;

interface Interface{
	combinationArray: number[][]
}

class Task117 extends FillArray117 implements Interface{
	combinationArray: number[][] = [];
	constructor(){
		//----------------------------------------
		super();
		this.combinationArray = this.fillArray();
		for(const combinationArrayElement of this.combinationArray){
			const numb = +combinationArrayElement.join("");
			if(this.isDivider(numb)){
				console.log(numb); // 2438195760
				// break;
				// 2438195760
				// 3785942160
				// 4753869120
				// 4876391520
			}
		}
		//----------------------------------------
	}
	//############################################################################
	//############################################################################
	//############################################################################
	//############################################################################
	private isDivider(numb: number){
		for(let i = 18; i >= 2; i--){
			if(numb % i !== 0)
				return false;
		}
		return true;
	}
}

//##########################################################################
//##########################################################################
//##########################################################################
//##########################################################################
new Task117();





