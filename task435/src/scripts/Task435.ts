interface IUser {
    id: number;
    name: string;
    getFullName(surname: string): string;
}

class User implements IUser{
    id: number;
    name: string;
    age: number;
    constructor(userId: number, userName: string, userAge: number) {
        this.id = userId;
        this.name = userName;
        this.age = userAge;
    }
    getFullName(): string {
        return this.name;
    }
}

let tom = new User(1, "Tom", 23);
// console.log(tom)
//----------------------------------------------------------
interface PackageUser {
    myArray:Array<Number>;
    // myArray2:Array<Object>;

    myArray2:Array<{
        id: number;
        name: string;
        age: number;
    }>;
}

// interface ActivatedPackageUser extends PackageUser {
//     age: number;
// }

class Task435 implements PackageUser{
    myArray: Array<Number> = [];
    myArray2: Array<{ id: number; name: string; age: number; }> = [];

}

let asd = new  Task435();
asd.myArray2.push(tom);
console.log(asd.myArray2[0].name)
















//----------------------------------------------------------
// const arr:any = [];
// arr.push(tom);

// console.log(arr[0].name)
// console.log(tom.getFullName());
//----------------------------------------------------------
// interface User{
//     id: number;
//     name: string;
// }
//
// interface ActivatedUser extends User{
//     age: number;
// }
//
// class Task435 implements ActivatedUser{
//     activatedUser: Array<ActivatedUser> = [];
//
//     public display(): void{
//
//     }
//
//
// }
//
// let task380 = new Task435()
// task380.display()
//--------------------------------------------

// interface Package {
//     id: number;
//     activated: boolean;
//     name: string;
// }
//
// interface ActivatedPackage extends Package {
//     activated: true;
//     type:string;
// }
//
// interface OptionalProps {
//     foo: string;
//     bar: number
// }
//
// const activatedPackages: Array< ActivatedPackage & Partial<OptionalProps> > = [
//     {
//         // обязательные поля
//         id: 234, // число
//         activated: true, // булевое
//         name: '45 days', // строка
//         // необязательные поля (несколько)
//         type: 'blablabla' // any
//     },
//     {
//         // обязательные поля
//         id: 123, // число
//         activated: true, // булевое
//         name: '30 days', // строка
//         // необязательные поля
//         type: 'blablabla' // any
//     },
// ];
//
// activatedPackages.forEach(item=>console.log(item.name));
//
//
// // class Task435{
// // //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// //     public display(): void{
// //         // 435. Скамья магистрата. Один мой приятель из Сингапура попросил меня некоторое
// //         // время назад решить следующую задачу. На скамье одного магистрата (где именно,
// //         //     неизвестно) занимают места два англичанина, два шотландца, два уэльсца, один француз,
// //         //     один итальянец, один испанец и один американец. Англичане не хотят сидеть рядом,
// //         //     шотландцы не хотят сидеть рядом и уэльсцы тоже не желают сидеть рядом друг с другом.
// //
// //         // Сколькими различными способами могут разместиться на скамье эти 10 человек так,
// //         //     чтобы никакие два человека одной и той же национальности не сидели рядом?
// //
// //         // const innerArray = this.arrayFun();
// //         // 2 ANG,
// //         // 2 SCOT,
// //         // 2 UELS,
// //         // 1 FRENCH,
// //         // 1 ITALIAN,
// //         // 1 ISPANIAN,
// //         // 1 USA
// //
// //         // const arrayObj:Array<Number> = [1,2,4,5];
// //
// //         const arrayObj:Array<Object>  = [
// //             {name:"Aza",age:34},
// //             {name:"Kamila",age:24}
// //         ];
// //
// //         arrayObj.forEach(item=> {
// //             console.log(item.name)
// //         })
// //
// //
// //         function printUser({name, age}: { name: string; age?: number }){
// //             return [name, age]
// //         }
// //
// //         let tom = {name: "Tom", age: 34};
// //         const name: any = printUser(tom);
// //         console.log(name)
// //
// //
// //     }
// //
// // //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// //     public arrayFun(): void{
// //         console.log(1111)
// //     }
// // }
// //
// // let task380 = new Task435()
// // task380.display()