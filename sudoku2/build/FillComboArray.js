"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FillComboArray = void 0;
const FillComboArray = (array, iterArray, referenceArray) => {
    const doubleArray = [];
    const bufferArray1 = [];
    const bufferArray2 = [];
    const bufferArray3 = [];
    const bufferArray4 = [];
    const bufferArray5 = [];
    const bufferArray6 = [];
    const bufferArray7 = [];
    const bufferArray8 = [];
    const bufferArray9 = [];
    // this.fillArray(this.array, this.len)
    let count = 0;
    if (0)
        for (let i = 0; i < array.length; i++) {
            console.log(array[i]);
        }
    if (1)
        array.forEach((item, index) => {
            // console.log(count++,"\t\t",item.toString());
            if (findDuplicateIndexes(item, referenceArray, 0, iterArray) &&
                findReferArrayIndexes(item, referenceArray, 0, iterArray))
                // console.log(item.join(" ")," <<<");
                bufferArray1.push(item);
            if (findDuplicateIndexes(item, referenceArray, 1, iterArray) &&
                findReferArrayIndexes(item, referenceArray, 1, iterArray))
                bufferArray2.push(item);
            if (findDuplicateIndexes(item, referenceArray, 2, iterArray) &&
                findReferArrayIndexes(item, referenceArray, 2, iterArray))
                bufferArray3.push(item);
            if (findDuplicateIndexes(item, referenceArray, 3, iterArray) &&
                findReferArrayIndexes(item, referenceArray, 3, iterArray))
                bufferArray4.push(item);
            if (findDuplicateIndexes(item, referenceArray, 4, iterArray) &&
                findReferArrayIndexes(item, referenceArray, 4, iterArray))
                bufferArray5.push(item);
            if (findDuplicateIndexes(item, referenceArray, 5, iterArray) &&
                findReferArrayIndexes(item, referenceArray, 5, iterArray))
                bufferArray6.push(item);
            if (findDuplicateIndexes(item, referenceArray, 6, iterArray) &&
                findReferArrayIndexes(item, referenceArray, 6, iterArray))
                bufferArray7.push(item);
            if (findDuplicateIndexes(item, referenceArray, 7, iterArray) &&
                findReferArrayIndexes(item, referenceArray, 7, iterArray))
                bufferArray8.push(item);
            if (findDuplicateIndexes(item, referenceArray, 8, iterArray) &&
                findReferArrayIndexes(item, referenceArray, 8, iterArray))
                bufferArray9.push(item);
        });
    doubleArray.push(bufferArray1);
    doubleArray.push(bufferArray2);
    doubleArray.push(bufferArray3);
    doubleArray.push(bufferArray4);
    doubleArray.push(bufferArray5);
    doubleArray.push(bufferArray6);
    doubleArray.push(bufferArray7);
    doubleArray.push(bufferArray8);
    doubleArray.push(bufferArray9);
    return doubleArray;
};
exports.FillComboArray = FillComboArray;
//########################################################################################
//########################################################################################
function findDuplicateIndexes(item, referenceArray, index, iterArray) {
    const isMatrix = referenceArray[index].map(item => !item);
    // console.log(isMatrix)
    // 0 2 9 0 4 0 5 1 8
    //[true,  false, false, true,  false, true, false, false, false]
    // │    0    │ 0 │ 2 │ 9 │ 0 │ 4 │ 0 │ 5 │ 1 │ 8 │
    // │    1    │ 4 │ 0 │ 0 │ 0 │ 1 │ 5 │ 0 │ 3 │ 0 │
    // │    2    │ 0 │ 0 │ 0 │ 8 │ 0 │ 0 │ 0 │ 0 │ 0 │
    // │    3    │ 6 │ 9 │ 3 │ 0 │ 5 │ 0 │ 8 │ 7 │ 0 │
    // │    4    │ 0 │ 5 │ 0 │ 4 │ 8 │ 0 │ 0 │ 0 │ 1 │
    // │    5    │ 0 │ 0 │ 0 │ 0 │ 0 │ 3 │ 0 │ 0 │ 0 │
    // │    6    │ 0 │ 4 │ 0 │ 0 │ 0 │ 0 │ 1 │ 2 │ 0 │
    // │    7    │ 0 │ 3 │ 2 │ 0 │ 0 │ 0 │ 0 │ 9 │ 0 │
    // │    8    │ 0 │ 0 │ 0 │ 6 │ 0 │ 2 │ 0 │ 0 │ 0 │
    for (let i1 = 0; i1 < referenceArray[index].length; i1++) {
        if (index === i1)
            continue;
        // console.log("-------------------------")
        for (let i2 = 0; i2 < isMatrix.length; i2++) {
            if (isMatrix[i2]) {
                if (referenceArray[i1][i2] === item[i2]) {
                    // console.log("["+i1+"]["+i2+"] ",isMatrix[i2])
                    // console.log( display(item,i2) );
                    // console.log( display(referenceArray[i1],i2) );
                    return false;
                }
            }
        }
    }
    return true;
}
function display(array, mainIndex) {
    let str = "";
    array.forEach((item, index) => {
        if (index === mainIndex)
            str += "[" + item + "] ";
        else
            str += item + " ";
    });
    return str.trim();
}
function findReferArrayIndexes(item, referenceArray, index, iterArray) {
    let isFlag = false;
    // console.log("index ",index);
    // console.log(referenceArray[index].join(" "));
    // console.log("length ",iterArray[index].length);
    // console.log(iterArray[index].join("  "));
    //
    // for(const iterArrayElement of iterArray[index]){
    //     console.log(iterArrayElement," | ",item[ iterArrayElement ] ,"\t", referenceArray[index][ iterArrayElement ])
    // }
    // console.log("--------------------------------")
    for (let i1 = 0; i1 < iterArray[index].length; i1++) {
        const innerIndex = iterArray[index][i1];
        // console.log(innerIndex);
        if (item[innerIndex] === referenceArray[index][innerIndex]) {
            isFlag = true;
        }
        else {
            return false;
        }
    }
    return isFlag;
}
//# sourceMappingURL=FillComboArray.js.map