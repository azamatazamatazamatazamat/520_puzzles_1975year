"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HoistingArray = void 0;
const HoistingArray = (iterArray) => {
    const innerArray = iterArray.concat();
    let buffer = [];
    for (let i1 = 0; i1 < innerArray.length - 1; i1++) {
        // console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
        for (let i2 = i1 + 1; i2 < innerArray.length; i2++) {
            // console.log("---------------------------------------")
            // console.table(innerArray)
            // console.log("i1 >> ",i1,"\t>> ",innerArray[i1]," ",innerArray[i1].length)
            // console.log("i2 >> ",i2,"\t>> ",innerArray[i2]," ",innerArray[i2].length)
            if (innerArray[i1].length < innerArray[i2].length) {
                buffer = innerArray[i2].concat();
                innerArray[i2] = innerArray[i1].concat();
                innerArray[i1] = buffer.concat();
            }
            buffer = null;
            // console.table(innerArray)
        }
    }
    return innerArray;
};
exports.HoistingArray = HoistingArray;
/*
before
[
  [ 1, 2, 4, 6, 7, 8 ],
  [ 0, 4, 5, 7 ],
  [ 3 ],
  [ 0, 1, 2, 4, 6, 7 ],
  [ 1, 3, 4, 8 ],
  [ 5 ],
  [ 1, 6, 7 ],
  [ 1, 2, 7 ],
  [ 3, 5 ]
]
//------------------------------------------------------
after
[
  [ 1, 2, 4, 6, 7, 8 ],
  [ 0, 1, 2, 4, 6, 7 ],
  [ 0, 4, 5, 7 ],
  [ 1, 3, 4, 8 ],
  [ 1, 6, 7 ],
  [ 1, 2, 7 ],
  [ 3, 5 ],
  [ 5 ],
  [ 3 ]
]*/
//# sourceMappingURL=HoistingArray.js.map