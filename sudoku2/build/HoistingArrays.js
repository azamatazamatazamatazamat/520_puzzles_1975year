"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hoistingArrays = void 0;
const hoistingArrays = (iterArray, referenceArray) => {
    const innerIterArray = iterArray.concat();
    const innerReferenceArray = referenceArray.concat();
    let iterBuffer = [];
    let referenceBuffer = [];
    for (let i1 = 0; i1 < innerIterArray.length - 1; i1++) {
        // console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
        for (let i2 = i1 + 1; i2 < innerIterArray.length; i2++) {
            // console.log("---------------------------------------")
            // console.table(innerArray)
            // console.log("i1 >> ",i1,"\t>> ",innerArray[i1]," ",innerArray[i1].length)
            // console.log("i2 >> ",i2,"\t>> ",innerArray[i2]," ",innerArray[i2].length)
            if (innerIterArray[i1].length < innerIterArray[i2].length) {
                iterBuffer = innerIterArray[i2].concat();
                referenceBuffer = innerReferenceArray[i2].concat();
                innerIterArray[i2] = innerIterArray[i1].concat();
                innerReferenceArray[i2] = innerReferenceArray[i1].concat();
                innerIterArray[i1] = iterBuffer.concat();
                innerReferenceArray[i1] = referenceBuffer.concat();
            }
            iterBuffer = null;
            referenceBuffer = null;
            // console.table(innerArray)
        }
    }
    return [innerIterArray, innerReferenceArray];
};
exports.hoistingArrays = hoistingArrays;
/*
before
[
  [ 1, 2, 4, 6, 7, 8 ],
  [ 0, 4, 5, 7 ],
  [ 3 ],
  [ 0, 1, 2, 4, 6, 7 ],
  [ 1, 3, 4, 8 ],
  [ 5 ],
  [ 1, 6, 7 ],
  [ 1, 2, 7 ],
  [ 3, 5 ]
]
//------------------------------------------------------
after
[
  [ 1, 2, 4, 6, 7, 8 ],
  [ 0, 1, 2, 4, 6, 7 ],
  [ 0, 4, 5, 7 ],
  [ 1, 3, 4, 8 ],
  [ 1, 6, 7 ],
  [ 1, 2, 7 ],
  [ 3, 5 ],
  [ 5 ],
  [ 3 ]
]*/
//# sourceMappingURL=HoistingArrays.js.map