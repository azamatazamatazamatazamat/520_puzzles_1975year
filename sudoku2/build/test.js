"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const groupBy_1 = __importDefault(require("lodash/groupBy"));
class Category {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}
class CategoryThread {
    constructor(threadId, category, title) {
        this.threadId = threadId;
        this.category = category;
        this.title = title;
    }
}
class ThreadItem {
    constructor(id, views, points, body, userName, userId, createdOn, threadId) {
        this.id = id;
        this.views = views;
        this.points = points;
        this.body = body;
        this.userName = userName;
        this.userId = userId;
        this.createdOn = createdOn;
        this.threadId = threadId;
    }
}
class Thread {
    constructor(id, views, title, body, userName, userId, points, createdOn, lastModifiedOn, threadItems, category) {
        this.id = id;
        this.views = views;
        this.title = title;
        this.body = body;
        this.userName = userName;
        this.userId = userId;
        this.points = points;
        this.createdOn = createdOn;
        this.lastModifiedOn = lastModifiedOn;
        this.threadItems = threadItems;
        this.category = category;
    }
}
if (0) {
    function getCategories() {
        return __awaiter(this, void 0, void 0, function* () {
            const promise = new Promise((res, rej) => {
                setTimeout(() => {
                    const categories = [];
                    categories.push(new Category("1", "Programming"));
                    categories.push(new Category("2", "Cooking"));
                    categories.push(new Category("3", "Sports"));
                    categories.push(new Category("4", "Entertainment"));
                    categories.push(new Category("5", "Travel"));
                    res(categories);
                }, 2000);
            });
            return promise;
        });
    }
    getCategories()
        .then((categories) => {
        categories.forEach((cat) => {
            console.log(cat.id, "\t\t", cat.name);
        });
    })
        .catch((err) => {
        console.log(err);
    });
}
//---------------------------------------------------------------------------
if (0) {
    function getTopCategories() {
        const promise = new Promise((res, rej) => {
            setTimeout(() => {
                const topCategories = [];
                const js = new CategoryThread("1", "Programming", "How can I learn JavaScript");
                topCategories.push(js);
                const node = new CategoryThread("2", "Programming", "How can I learn Node");
                topCategories.push(node);
                const react = new CategoryThread("3", "Programming", "How can I learn React");
                topCategories.push(react);
                const french = new CategoryThread("4", "Cooking", "How do I learn French cuisine?");
                topCategories.push(french);
                const italian = new CategoryThread("5", "Cooking", "How do I learn Italian cuisine?");
                topCategories.push(italian);
                const soccer = new CategoryThread("6", "Sports", "How can I learn to play Soccer");
                topCategories.push(soccer);
                const basketball = new CategoryThread("7", "Sports", "How can I learn to play Basketball");
                topCategories.push(basketball);
                const baseball = new CategoryThread("8", "Sports", "How can I learn to play Baseball");
                topCategories.push(baseball);
                res(topCategories);
            }, 2000);
        });
        return promise;
    }
    getTopCategories()
        .then((res) => {
        const topCatThreads = (0, groupBy_1.default)(res, "category");
        //
        // const topElements = [];
        // for(let key in topCatThreads){
        //     console.log(key)
        //     // const currentTop = topCatThreads[key];
        //     // topElements.push(<TopCategorykey={key}topCategories={currentTop}/>);
        // }
        console.log(JSON.stringify(topCatThreads.Programming, null, 2));
        console.log("::::::::::::::::::::::::::::::::");
        console.log(JSON.stringify(res, null, 2));
        // setTopCategories(topElements);
    });
}
//---------------------------------------------------------------------------
if (0) {
    function getThreadById(Id) {
        const promise = new Promise((res, rej) => {
            setTimeout(() => {
                const thread = {
                    id: "1",
                    views: 22,
                    title: "Thread 1",
                    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    userName: "dave",
                    userId: "1",
                    points: 11,
                    createdOn: new Date(),
                    lastModifiedOn: new Date(),
                    threadItems: [
                        {
                            id: "1",
                            views: 22,
                            points: 2,
                            body: "ThreadItem 1",
                            userName: "jon",
                            userId: "2",
                            createdOn: new Date(),
                            threadId: "1",
                        },
                        {
                            id: "2",
                            views: 11,
                            points: 14,
                            body: "ThreadItem 2",
                            userName: "linda",
                            userId: "4",
                            createdOn: new Date(),
                            threadId: "1",
                        },
                    ],
                    category: new Category("1", "Programming"),
                };
                res(thread);
            }, 2000);
        });
        return promise;
    }
    getThreadById("2").then((item) => {
        console.log(item);
    });
}
if (1) {
    const isPasswordValid = (password) => {
        const passwordTestResult = {
            message: "",
            isValid: true,
        };
        if (password.length < 8) {
            passwordTestResult.message = "Password must be at least 8 characters";
            passwordTestResult.isValid = false;
            return passwordTestResult;
        }
        const strongPassword = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})");
        if (!strongPassword.test(password)) {
            passwordTestResult.message =
                "Password must contain at least 1 special character, 1 cap letter, and 1 number";
            passwordTestResult.isValid = false;
        }
        return passwordTestResult;
    };
    class UserResult {
        constructor(messages, user) {
            this.messages = messages;
            this.user = user;
        }
    }
    const register = (email, userName, password) => __awaiter(void 0, void 0, void 0, function* () {
        const result = isPasswordValid(password);
        if (!result.isValid) { //"Test123!@#" === true /___/ "test123!@#" === false
            return {
                messages: [
                    "Passwords must have min length 8, 1 upper " +
                        "character, 1 number, and 1 symbol",
                ],
            };
        }
        const userEntity = {
            email: 'next_test2@gmail.com',
            userName: 'next_test2',
            password: '$2a$10$9KdejVHzf7EXQgh6i0B4YuNpr6W7xAqXIt3ehMccUesS5y.r1vOkW',
        };
        userEntity.password = "";
        return {
            user: userEntity,
        };
    });
    //-------------------------------------------------
    // {
    //     "userName":"next_test",
    //     "email":"next_test@gmail.com",
    //     "password":"Test123!@#"
    // }
    const main = () => __awaiter(void 0, void 0, void 0, function* () {
        //     "userName":"next_test",
        //     "email":"next_test@gmail.com",
        //     "password":"Test123!@#"
        const userResult = yield register("next_test@gmail.com", "next_test", "Test123!@#");
        console.log(userResult);
    });
    main();
    console.log(1111);
    //###########################################################################
    //###########################################################################
    //###########################################################################
    //###########################################################################
}
//# sourceMappingURL=test.js.map