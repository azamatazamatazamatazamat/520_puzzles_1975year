"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FillArray = void 0;
class FillArray {
    constructor() { }
    isEqual(x1, ...x2) {
        return x2.some(item => item === x1);
    }
    fillArray(array, len) {
        // fillArray(len: number = 9):void{
        for (let i1 = 1; i1 <= len; i1++) {
            for (let i2 = 1; i2 <= len; i2++) {
                if (this.isEqual(i2, i1))
                    continue;
                for (let i3 = 1; i3 <= len; i3++) {
                    if (this.isEqual(i3, i1, i2))
                        continue;
                    for (let i4 = 1; i4 <= len; i4++) {
                        if (this.isEqual(i4, i1, i2, i3))
                            continue;
                        for (let i5 = 1; i5 <= len; i5++) {
                            if (this.isEqual(i5, i1, i2, i3, i4))
                                continue;
                            for (let i6 = 1; i6 <= len; i6++) {
                                if (this.isEqual(i6, i1, i2, i3, i4, i5))
                                    continue;
                                for (let i7 = 1; i7 <= len; i7++) {
                                    if (this.isEqual(i7, i1, i2, i3, i4, i5, i6))
                                        continue;
                                    for (let i8 = 1; i8 <= len; i8++) {
                                        if (this.isEqual(i8, i1, i2, i3, i4, i5, i6, i7))
                                            continue;
                                        for (let i9 = 1; i9 <= len; i9++) {
                                            if (this.isEqual(i9, i1, i2, i3, i4, i5, i6, i7, i8))
                                                continue;
                                            array.push([i1, i2, i3, i4, i5, i6, i7, i8, i9]);
                                            // console.log(`${i1} ${i2} ${i3} ${i4} ${i5} ${i6} ${i7} ${i8} ${i9}`)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } //referenceArray
    }
}
exports.FillArray = FillArray;
// const innerObj = new FillArray();
// innerObj.fillArray()
//# sourceMappingURL=FillArray.js.map