import {FillArray} from "./FillArray";
import {fillIterArray} from "./FillIterArray";
import {FillComboArray} from "./FillComboArray";
import {filterArray} from "./FilterArray";
import {hoistingArrays} from "./HoistingArrays";

interface ISudoku{
    // constNum: number;
    array: number[][];
    // mapIterArray: number[][];
    comboArray: number[][][];
    referenceArray: number[][];
    len: number;
    bigNum: number;
}

class Sudoku extends FillArray implements ISudoku{
    // constNum: number = 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9;
    array: number[][] = [];
    // mapIterArray: number[][] = [];
    comboArray: number[][][] = [];
    referenceArray: number[][] = [];
    len: number;
    bigNum: number = 0;

    constructor(len: number, referenceArray: number[][]){
        super();
        this.len = len;
        this.referenceArray = referenceArray;
    }

    mainFunction(){
        let cloneArray: number[][] = [];
        this.fillArray(this.array, this.len);
        // console.table(this.array);

        let secondMapIterArray = fillIterArray(this.referenceArray);
        // const firstMapIterArray = secondMapIterArray.concat();
        // console.table(secondMapIterArray);
        // console.table(firstMapIterArray);

        // const allArrays = hoistingArrays(secondMapIterArray, this.referenceArray);
        // secondMapIterArray = allArrays[0];
        // cloneArray = allArrays[1];

        // console.table(secondMapIterArray);
        // console.table(firstMapIterArray);

        // console.table(cloneArray);
        console.table(this.referenceArray);
        //----------------------------------------------------------
        console.table(secondMapIterArray);
        this.comboArray = FillComboArray(this.array, secondMapIterArray, this.referenceArray);
        // console.table(cloneArray);
        // console.table(this.referenceArray);
        // this.testDisplay("3 1 5 8 2 7 9 4 6",0);
        // this.testDisplay("4 6 8 9 1 5 7 3 2",1);
        // this.testDisplay("7 2 9 3 4 6 5 1 8",2);
        // this.testDisplay("9 4 6 5 3 8 1 2 7",3);
        // this.testDisplay("5 7 1 6 9 2 4 8 3",4);
        // this.testDisplay("8 3 2 1 7 4 6 9 5",5);
        // this.testDisplay("6 9 3 2 5 1 8 7 4",6);
        // this.testDisplay("2 5 7 4 8 9 3 6 1",7);
        // this.testDisplay("1 8 4 7 6 3 2 5 9",8);
        // console.table(this.comboArray[1]);
        // console.table(this.comboArray[2]);

        if(0)
        for(let i = 0; i < this.comboArray.length; i++){
            console.log(i+1,"\t",this.comboArray[i].length);
        }

        // filterArray({
        //         comboArray:this.comboArray,
        //         constNum: this.constNum
        //     });

        // console.log( this.comboArray[0].length )
        filterArray(this.comboArray);
    }

    testDisplay(str:string,index:number){
        this.comboArray[index].some(item=>{
            const innerStr = item.join(" ")
            if(innerStr === str){
                console.log("> ",innerStr);
                return true;
            }
        })
        return "none";
    }
}

const globalArray: number[][] = [
    // [0, 0, 0, 8, 0, 0, 0, 0, 0],//    [ 3 ],
    // [4, 0, 0, 0, 1, 5, 0, 3, 0],//    [ 0, 4, 5, 7 ],
    // [0, 2, 9, 0, 4, 0, 5, 1, 8],//    [ 1, 2, 4, 6, 7, 8 ],
    // [0, 4, 0, 0, 0, 0, 1, 2, 0],//    [ 1, 6, 7 ],
    // [0, 0, 0, 6, 0, 2, 0, 0, 0],//    [ 3, 5 ],
    // [0, 3, 2, 0, 0, 0, 0, 9, 0],//    [ 1, 2, 7 ],
    // [6, 9, 3, 0, 5, 0, 8, 7, 0],//    [ 0, 1, 2, 4, 6, 7 ],
    // [0, 5, 0, 4, 8, 0, 0, 0, 1],//    [ 1, 3, 4, 8 ],
    // [0, 0, 0, 0, 0, 3, 0, 0, 0],//    [ 5 ]

    // "3 1 5 8 2 7 9 4 6"
    // "4 6 8 9 1 5 7 3 2"
    // "7 2 9 3 4 6 5 1 8"
    // "9 4 6 5 3 8 1 2 7"
    // "5 7 1 6 9 2 4 8 3"
    // "8 3 2 1 7 4 6 9 5"
    // "6 9 3 2 5 1 8 7 4"
    // "2 5 7 4 8 9 3 6 1"
    // "1 8 4 7 6 3 2 5 9"

    // уровень легкий
    [2, 0, 0,  1, 0, 0,  3, 0, 8],//2 5 4 1 9 7 3 6 8
    [0, 0, 7,  3, 4, 0,  0, 0, 1],//6 8 7 3 4 2 5 9 1
    [0, 0, 0,  0, 6, 0,  0, 0, 0],//9 1 3 5 6 8 7 4 2
    [3, 4, 5,  0, 0, 0,  0, 0, 0],//3 4 5 9 2 6 1 8 7
    [0, 0, 0,  8, 0, 1,  0, 0, 0],//7 2 6 8 5 1 9 3 4
    [8, 0, 0,  0, 7, 3,  0, 0, 0],//8 9 1 4 7 3 6 2 5
    [0, 0, 0,  0, 0, 5,  0, 0, 3],//4 7 9 6 8 5 2 1 3
    [0, 6, 2,  0, 3, 0,  0, 5, 0],//1 6 2 7 3 4 8 5 9
    [0, 0, 8,  0, 0, 0,  4, 0, 0],//5 3 8 2 1 9 4 7 6


    // уровень сложный
    // [3, 0, 0,  0, 6, 0,  0, 1, 0],//3 7 8 9 6 2 4 1 5
    // [0, 0, 9,  1, 8, 0,  0, 6, 3],//4 2 9 1 8 5 7 6 3
    // [5, 6, 0,  0, 0, 4,  0, 0, 0],//5 6 1 3 7 4 9 2 8
    // [0, 0, 0,  0, 0, 0,  8, 0, 0],//2 5 7 6 4 9 8 3 1
    // [0, 8, 0,  0, 3, 1,  6, 0, 0],//9 8 4 5 3 1 6 7 2
    // [0, 0, 0,  0, 0, 7,  5, 4, 9],//6 1 3 8 2 7 5 4 9
    // [0, 0, 0,  4, 9, 0,  0, 0, 0],//8 3 2 4 9 6 1 5 7
    // [7, 4, 0,  0, 0, 8,  0, 9, 0],//7 4 5 2 1 8 3 9 6
    // [1, 0, 0,  0, 5, 0,  0, 0, 0],//1 9 6 7 5 3 2 8 4

    // уровень экперт
    // [0, 7, 8,  5, 0, 0,  0, 0, 0],//1 7 8 5 4 3 9 6 2
    // [0, 0, 3,  0, 0, 7,  8, 0, 0],//9 4 3 6 2 7 8 1 5
    // [0, 0, 0,  1, 9, 0,  0, 0, 0],//6 5 2 1 9 8 4 3 7
    // [0, 0, 7,  0, 0, 0,  2, 9, 0],//4 6 7 8 3 5 2 9 1
    // [0, 9, 0,  0, 6, 1,  0, 4, 0],//8 9 5 2 6 1 7 4 3
    // [0, 0, 0,  0, 0, 4,  0, 0, 0],//2 3 1 9 7 4 6 5 8
    // [3, 0, 6,  0, 0, 2,  0, 0, 0],//3 8 6 4 5 2 1 7 9
    // [0, 1, 0,  0, 0, 0,  0, 0, 4],//5 1 9 7 8 6 3 2 4
    // [0, 0, 0,  0, 0, 0,  5, 0, 0],//7 2 4 3 1 9 5 8 6

    // уровень безумный/дьявольский
    // [0, 6, 5,  4, 0, 0,  0, 0, 0],//2 6 5 4 7 8 3 1 9
    // [0, 3, 0,  0, 0, 5,  0, 7, 6],//4 3 1 9 2 5 8 7 6
    // [0, 0, 0,  0, 0, 0,  0, 2, 0],//9 8 7 6 1 3 5 2 4
    // [7, 0, 0,  8, 0, 0,  0, 6, 1],//7 4 3 8 5 2 9 6 1
    // [0, 0, 0,  0, 0, 6,  2, 0, 0],//5 9 8 1 4 6 2 3 7
    // [0, 1, 0,  0, 0, 0,  4, 0, 0],//6 1 2 7 3 9 4 8 5
    // [0, 7, 0,  0, 0, 4,  0, 5, 3],//1 7 9 2 8 4 6 5 3
    // [0, 0, 0,  0, 0, 0,  1, 0, 0],//3 2 4 5 6 7 1 9 8
    // [8, 0, 0,  0, 9, 0,  0, 0, 0],//8 5 6 3 9 1 7 4 2

]

// let fillArray = new fillArray();
let sudoku = new Sudoku(9, globalArray);
sudoku.mainFunction();
// sudoku.test();
// sudoku.mainFunction2();