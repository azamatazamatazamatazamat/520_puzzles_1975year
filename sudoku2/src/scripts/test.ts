import groupBy from "lodash/groupBy";
class Category{
    constructor(
        public id: string,
        public name: string
    ){}
}
class CategoryThread{
    constructor(
        public threadId: string,
        public category: string,
        public title: string
    ){}
}
class ThreadItem{
    constructor(
        public id: string,
        public views: number,
        public points: number,
        public body: string,
        public userName: string,
        public userId: string,
        public createdOn: Date,
        public threadId: string
    ){}
}
class Thread{
    constructor(
        public id: string,
        public views: number,
        public title: string,
        public body: string,
        public userName: string,
        public userId: string,
        public points: number,
        public createdOn: Date,
        public lastModifiedOn: Date,
        public threadItems: ThreadItem[],
        public category: Category
    ){}
}
if(0){
    async function getCategories(): Promise<Category[]>{
        const promise = new Promise<Category[]>((res, rej)=>{
            setTimeout(()=>{
                const categories = [];
                categories.push(new Category("1", "Programming"));
                categories.push(new Category("2", "Cooking"));
                categories.push(new Category("3", "Sports"));
                categories.push(new Category("4", "Entertainment"));
                categories.push(new Category("5", "Travel"));
                res(categories);
            }, 2000);
        });
        return promise;
    }

    getCategories()
        .then((categories: Category[])=>{
            categories.forEach((cat)=>{
                console.log(cat.id, "\t\t", cat.name);
            });
        })
        .catch((err)=>{
            console.log(err);
        });
}
//---------------------------------------------------------------------------
if(0){
    function getTopCategories(): Promise<Array<CategoryThread>>{
        const promise = new Promise<Array<CategoryThread>>((res, rej)=>{
            setTimeout(()=>{
                const topCategories = [];
                const js = new CategoryThread("1", "Programming", "How can I learn JavaScript");
                topCategories.push(js);
                const node = new CategoryThread("2", "Programming", "How can I learn Node");
                topCategories.push(node);
                const react = new CategoryThread("3", "Programming", "How can I learn React");
                topCategories.push(react);
                const french = new CategoryThread("4", "Cooking", "How do I learn French cuisine?");
                topCategories.push(french);
                const italian = new CategoryThread("5", "Cooking", "How do I learn Italian cuisine?");
                topCategories.push(italian);
                const soccer = new CategoryThread("6", "Sports", "How can I learn to play Soccer");
                topCategories.push(soccer);
                const basketball = new CategoryThread("7", "Sports", "How can I learn to play Basketball");
                topCategories.push(basketball);
                const baseball = new CategoryThread("8", "Sports", "How can I learn to play Baseball");
                topCategories.push(baseball);

                res(topCategories);
            }, 2000);
        });
        return promise;
    }

    getTopCategories()
        .then((res)=>{
            const topCatThreads = groupBy(res, "category");
            //
            // const topElements = [];
            // for(let key in topCatThreads){
            //     console.log(key)
            //     // const currentTop = topCatThreads[key];
            //     // topElements.push(<TopCategorykey={key}topCategories={currentTop}/>);
            // }
            console.log(JSON.stringify(topCatThreads.Programming, null, 2))
            console.log("::::::::::::::::::::::::::::::::")
            console.log(JSON.stringify(res, null, 2))
            // setTopCategories(topElements);
        });
}
//---------------------------------------------------------------------------
if(0){
    function getThreadById(Id: string): Promise<Thread>{
        const promise = new Promise<Thread>((res, rej)=>{
            setTimeout(()=>{
                const thread = {
                    id: "1",
                    views: 22,
                    title: "Thread 1",
                    body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    userName: "dave",
                    userId: "1",
                    points: 11,
                    createdOn: new Date(),
                    lastModifiedOn: new Date(),
                    threadItems: [
                        {
                            id: "1",
                            views: 22,
                            points: 2,
                            body: "ThreadItem 1",
                            userName: "jon",
                            userId: "2",
                            createdOn: new Date(),
                            threadId: "1",
                        },
                        {
                            id: "2",
                            views: 11,
                            points: 14,
                            body: "ThreadItem 2",
                            userName: "linda",
                            userId: "4",
                            createdOn: new Date(),
                            threadId: "1",
                        },
                    ],
                    category: new Category("1", "Programming"),
                };

                res(thread);
            }, 2000);
        });
        return promise;
    }

    getThreadById("2").then((item)=>{
        console.log(item)
    });
}
if(1){
//###########################################################################
//###########################################################################
//###########################################################################
//###########################################################################
    interface PasswordTestResult{
        message: string;
        isValid: boolean;
    }

    const isPasswordValid = (password: string): PasswordTestResult=>{
        const passwordTestResult: PasswordTestResult = {
            message: "",
            isValid: true,
        };

        if(password.length < 8){
            passwordTestResult.message = "Password must be at least 8 characters";
            passwordTestResult.isValid = false;
            return passwordTestResult;
        }

        const strongPassword = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})");
        if(!strongPassword.test(password)){
            passwordTestResult.message =
                "Password must contain at least 1 special character, 1 cap letter, and 1 number";
            passwordTestResult.isValid = false;
        }

        return passwordTestResult;
    };
//----------------------------------------------------------------------------------
    type UserT = {
        email:string,
        userName:string,
        password:string
    }
    class UserResult{
        constructor(public messages?: Array<string>,public user?: UserT){}
    }

    const register = async(
        email: string,
        userName: string,
        password: string
    ): Promise<UserResult>=>{

        const result = isPasswordValid(password);
        if(!result.isValid){//"Test123!@#" === true /___/ "test123!@#" === false
            return {
                messages: [
                    "Passwords must have min length 8, 1 upper " +
                    "character, 1 number, and 1 symbol",
                ],
            };
        }

        const userEntity = {
            email: 'next_test2@gmail.com',
            userName: 'next_test2',
            password: '$2a$10$9KdejVHzf7EXQgh6i0B4YuNpr6W7xAqXIt3ehMccUesS5y.r1vOkW',
        }

        userEntity.password = "";
        return {
            user: userEntity,
        }
    };
    //-------------------------------------------------
    // {
    //     "userName":"next_test",
    //     "email":"next_test@gmail.com",
    //     "password":"Test123!@#"
    // }
    const main = async ()=>{
        //     "userName":"next_test",
        //     "email":"next_test@gmail.com",
        //     "password":"Test123!@#"
        const userResult = await register(
            "next_test@gmail.com",
            "next_test",
            "Test123!@#"
        );

        console.log(userResult)
    }
    main()
    console.log(1111)
//###########################################################################
//###########################################################################
//###########################################################################
//###########################################################################
}

