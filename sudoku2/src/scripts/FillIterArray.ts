export const fillIterArray = (referenceArray: number[][])=>{
    const innerArray: number[][] = [];
    for(let i1 = 0; i1 < referenceArray.length; i1++){
        innerArray.push(findIndexes(referenceArray, i1));
    }
    return innerArray;
}

function findIndexes(referenceArray: number[][], index: number){
    const innerArray: number[] = [];
    for(let i1 = 0; i1 < referenceArray[index].length; i1++){
        if(referenceArray[index][i1] === 0) continue;
        // innerArray.push([i1,referenceArray[index][i1]]);
        // innerArray.push(referenceArray[index][i1]);
        innerArray.push(i1);
    }
    return innerArray;//[ [ 0, 6 ], [ 3, 3 ], [ 5, 9 ] ]
}
/*
[0, 2, 9,   0, 4, 0,   5, 1, 8],    [ 1, 2, 4, 6, 7, 8 ],
[4, 0, 0,   0, 1, 5,   0, 3, 0],    [ 0, 4, 5, 7 ],
[0, 0, 0,   8, 0, 0,   0, 0, 0],    [ 3 ],

[6, 9, 3,   0, 5, 0,   8, 7, 0],    [ 0, 1, 2, 4, 6, 7 ],
[0, 5, 0,   4, 8, 0,   0, 0, 1],    [ 1, 3, 4, 8 ],
[0, 0, 0,   0, 0, 3,   0, 0, 0],    [ 5 ]

[0, 4, 0,   0, 0, 0,   1, 2, 0],    [ 1, 6, 7 ],
[0, 3, 2,   0, 0, 0,   0, 9, 0],    [ 1, 2, 7 ],
[0, 0, 0,   6, 0, 2,   0, 0, 0],    [ 3, 5 ],

//------------------------------------------------

7  2  9    3  4  6    5  1  8
4  6  8    9  1  5    7  3  2
3  1  5    8  2  7    9  4  6

6  9  3    2  5  1    8  7  4
2  5  7    4  8  9    3  6  1
1  8  4    7  6  3    2  5  9

9  4  6    5  3  8    1  2  7
8  3  2    1  7  4    6  9  5
5  7  1    6  9  2    4  8  3




 */