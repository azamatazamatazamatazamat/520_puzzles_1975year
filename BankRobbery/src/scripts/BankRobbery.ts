interface ICombo{
	accComboArray: number[][],
}

class BankRobbery implements ICombo{
	accComboArray: number[][] = [];
	constructor(){
		const mainArray = this.accComboArray;
		mainArray.push([1, 1, 1, 1, 1, 1, 1, 1]);
		for(let i = 0; true; i++){
			const innerArray = this.returnInnerArray(mainArray);
			if(innerArray){
				mainArray.push(innerArray);
				const [s1, s2, s3, s4, s5, s6, s7, s8] = innerArray;
				console.log(`${i} _  ${s1} ${s2} ${s3} ${s4} ${s5} ${s6} ${s7} | ${s8}`)
			}else
				break;
		}
		// console.table(mainArray);
	}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	/*
		1 1 1 1 1 1 1 | 1
		1 1 1 1 1 1 0 | 0
		1 1 1 1 0 1 0 | 1
		1 1 1 1 0 1 1 | 0
	*/
	private returnInnerArray(mainArray: number[][]){
		if(mainArray.at(-1)){
			const innerArray = mainArray.at(-1)!.concat();

			if(innerArray.at(-1) === 1){
				innerArray[innerArray.length - 1] = 0;//<<<<<<<<<<<<<<<<
				innerArray[innerArray.length - 2] = !innerArray[innerArray.length - 2] ? 1 : 0;
				return innerArray;
			}else{
//------------------------------------------------------------------------
				for(let i = innerArray.length - 1; i > 0; i--){
					const rightIndex = i;
					const leftIndex = i - 1;
					// console.log("[", leftIndex, "] ", innerArray[leftIndex], " | ", innerArray[rightIndex], " [", rightIndex, "]");

					if(innerArray[rightIndex]){
						innerArray[innerArray.length - 1] = 1;//<<<<<<<<<<<<<<<<
						innerArray[leftIndex] = !innerArray[leftIndex] ? 1 : 0;
						return innerArray;
					}
				}
//------------------------------------------------------------------------
			}//for
		}else{
			return null;
		}
		//--------------------------
	}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
}

//############################################################################
//############################################################################
//############################################################################
//############################################################################
new BankRobbery();
