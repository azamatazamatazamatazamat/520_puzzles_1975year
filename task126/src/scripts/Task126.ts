import {FillArray126} from "./FillArray126";

type Operation = (x: number)=>number;

interface Interface{
	combinationArray: number[][]
}

class Task126 extends FillArray126 implements Interface{
	combinationArray: number[][] = [];
	constructor(){
		//----------------------------------------
		super();
		let count = 0;
		const limit = 362880 / 2 + 10;
		this.combinationArray = this.fillArray();
		let totalSum = 0;
		for(const elem of this.combinationArray){
			count++;
			// if(count === limit) break;
				totalSum += elem.reduce((acc,item)=>acc+item,0)
				console.log(totalSum);// 16_329_600
		}
		//----------------------------------------
	}
	//############################################################################
	//############################################################################
	//############################################################################
	//############################################################################
	private isDivider(numb: number){
		for(let i = 18; i >= 2; i--){
			if(numb % i !== 0)
				return false;
		}
		return true;
	}
}

//##########################################################################
//##########################################################################
//##########################################################################
//##########################################################################
new Task126();





