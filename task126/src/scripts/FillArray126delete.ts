type TObj = { [key: string]: number };

export class FillArray126delete{
	constructor(){
	}

	fillArray(): void | number[][]{
		const array: number[][] = [];
		const start = 1;
		const len: number = 9;

		for(let i1 = start; i1 <= len; i1++){
		// if(this.isEqual(i1)) continue;
		//------------------
		for(let i2 = start; i2 <= len; i2++){
		if(this.isEqual(i2, i1)) continue;
		//------------------
		for(let i3 = start; i3 <= len; i3++){
		if(this.isEqual(i3, i1, i2)) continue;
		//------------------
		for(let i4 = start; i4 <= len; i4++){
		if(this.isEqual(i4, i1, i2, i3)) continue;
		//------------------
		for(let i5 = start; i5 <= len; i5++){
		if(this.isEqual(i5, i1, i2, i3, i4)) continue;
		//------------------
		for(let i6 = start; i6 <= len; i6++){
		if(this.isEqual(i6, i1, i2, i3, i4, i5)) continue;
		//------------------
		for(let i7 = start; i7 <= len; i7++){
		if(this.isEqual(i7, i1, i2, i3, i4, i5, i6)) continue;
		//------------------
		for(let i8 = start; i8 <= len; i8++){
		if(this.isEqual(i8, i1, i2, i3, i4, i5, i6, i7)) continue;
		//------------------
		for(let i9 = start; i9 <= len; i9++){
		if(this.isEqual(i9, i1, i2, i3, i4, i5, i6, i7, i8)) continue;
		//------------------
		// console.log(`${i1} ${i2} ${i3} ${i4} ${i5} ${i6} ${i7} ${i8}`)
			array.push([i1, i2, i3, i4, i5, i6, i7, i8, i9]);
		}}}}}}}}}
		return array;
	}

	protected isEqual(x1: number, ...x2: number[]){
		return x2.some(item=>item === x1)
	}
}

const fillArray117 = new FillArray126delete();
const mainArray = fillArray117.fillArray() as number[][];
separateValue(mainArray);
console.table(mainArray);


//################################################################################
//################################################################################
//################################################################################
//################################################################################

function separateValue(mainArray: number[][]){
	let innerLen = mainArray.length - 1;
	z1:while(true){
		z2:for(let i1 = innerLen; i1 >= 0; i1--){
			if(i1 === 0) break z1;
			const tallElemStr = mainArray[i1].concat().reverse().join("");
			const isShow = false;
			if(isShow) console.log("######################################################");
			if(isShow) console.log(`${mainArray.length - 5}    [${mainArray.at(-5)}]`);
			if(isShow) console.log(`${mainArray.length - 4}    [${mainArray.at(-4)}]`);
			if(isShow) console.log(`${mainArray.length - 3}    [${mainArray.at(-3)}]`);
			if(isShow) console.log(`${mainArray.length - 2}    [${mainArray.at(-2)}]`);
			if(isShow) console.log(`${mainArray.length - 1}    [${mainArray.at(-1)}]`);
			//--------------------------------------
			const innerPosit = binarySearch(mainArray, tallElemStr);
			if(innerPosit !== -1){
				// console.log(mainArray[innerPosit]);
				mainArray.splice(i1, 1);
				innerLen = i1 - 1;
			}
			//--------------------------------------

			if(isShow) console.log(">>> ", i1)
			if(isShow) console.log(mainArray.length);
		}
	}

//----------------------------------------------------------
	function binarySearch(sortedArray:number[][], key:string){
		let start = 0;
		let end = sortedArray.length - 1;

		while (start <= end) {
			let middle = Math.floor((start + end) / 2);
			const middlePosition = sortedArray[middle].join("");

			if (middlePosition === key) {
				return middle;
			} else if (middlePosition < key) {
				start = middle + 1;
			} else {
				end = middle - 1;
			}
		}
		return -1;
	}
}