"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FillArray126_1 = require("./FillArray126");
class Task126 extends FillArray126_1.FillArray126 {
    constructor() {
        //----------------------------------------
        super();
        this.combinationArray = [];
        let count = 0;
        const limit = 362880 / 2 + 10;
        this.combinationArray = this.fillArray();
        let totalSum = 0;
        for (const elem of this.combinationArray) {
            count++;
            // if(count === limit) break;
            totalSum += elem.reduce((acc, item) => acc + item, 0);
            console.log(totalSum); // 16_329_600
        }
        //----------------------------------------
    }
    //############################################################################
    //############################################################################
    //############################################################################
    //############################################################################
    isDivider(numb) {
        for (let i = 18; i >= 2; i--) {
            if (numb % i !== 0)
                return false;
        }
        return true;
    }
}
//##########################################################################
//##########################################################################
//##########################################################################
//##########################################################################
new Task126();
//# sourceMappingURL=Task126.js.map