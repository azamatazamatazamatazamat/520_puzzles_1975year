// 229. Головоломка с почтовыми марками. Одного юнца, собиравшего марки, спросили,
// сколько марок в его альбоме, на что он ответил:
// — Если число марок разделить на
// 2, то в остатке получится 1; если разделить на
// 3, то в остатке получится 2; если разделить на
// 4, то в остатке получится 3; если разделить на
// 5, то в остатке получится 4; если разделить на
// 6, то в остатке получится 5; если разделить на
// 7, то в остатке получится 6; если разделить на
// 8, то в остатке получится 7; если разделить на
// 9, то в остатке получится 8; если разделить на
// 10, то в остатке получится 9. Всего в альбоме меньше
// 3000 марок.
// remainder of the division

interface IMarks{
    division: number[];
    remainder: number[];
}
const totalMarks: number = 3000;
const objMarks: IMarks = {
    division: [2, 3, 4, 5, 6, 7, 8, 9, 10],
    remainder: [1, 2, 3, 4, 5, 6, 7, 8, 9]
}
const maxNumber:number = Math.max.apply(null, objMarks.division);
const globalArray:Array<boolean> = new Array(objMarks.division.length)

globalArray.fill(false, 0, globalArray.length)


    if(1)
    // z1: for(let i1 = totalMarks; i1 >= maxNumber; i1--){
        z1: for(let i1 = totalMarks; maxNumber !== i1  ; i1--){
        globalArray.fill(false, 0, globalArray.length)

        for(let i2 = 0; i2 < objMarks.division.length; i2++){
        const division: number = objMarks.division[i2]
        const remainder: number = objMarks.remainder[i2]

            if(i1 % division !== remainder) continue z1;

            // console.log(i1, " %", division, " === ", remainder)
            globalArray[i2] = true;
        }
        if(globalArray.every(n => n===true))
            console.log(i1);
    }

