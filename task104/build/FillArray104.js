"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FillArray104 = void 0;
class FillArray104 {
    constructor() {
    }
    fillArray() {
        //1, 2, 3, 4, 5, 7, 8, 9
        const len = 9;
        const array = [];
        for (let i1 = 1; i1 <= len; i1++) {
            if (this.isEqual(i1, 6))
                continue;
            for (let i2 = 1; i2 <= len; i2++) {
                if (this.isEqual(i2, i1, 6))
                    continue;
                //------------------
                for (let i3 = 1; i3 <= len; i3++) {
                    if (this.isEqual(i3, i1, i2, 6))
                        continue;
                    //------------------
                    for (let i4 = 1; i4 <= len; i4++) {
                        if (this.isEqual(i4, i1, i2, i3, 6))
                            continue;
                        //------------------
                        for (let i5 = 1; i5 <= len; i5++) {
                            if (this.isEqual(i5, i1, i2, i3, i4, 6))
                                continue;
                            //------------------
                            for (let i6 = 1; i6 <= len; i6++) {
                                if (this.isEqual(i6, i1, i2, i3, i4, i5, 6))
                                    continue;
                                //------------------
                                for (let i7 = 1; i7 <= len; i7++) {
                                    if (this.isEqual(i7, i1, i2, i3, i4, i5, i6, 6))
                                        continue;
                                    //------------------
                                    for (let i8 = 1; i8 <= len; i8++) {
                                        if (this.isEqual(i8, i1, i2, i3, i4, i5, i6, i7, 6))
                                            continue;
                                        //------------------
                                        // for(let i9 = 1; i9 <= len; i9++){
                                        //     if(this.isEqual(i9,i1,i2,i3,i4,i5,i6,i7,i8)) continue;
                                        array.push([i1, i2, i3, i4, i5, i6, i7, i8]);
                                        // console.log(`${i1} ${i2} ${i3} ${i4} ${i5} ${i6} ${i7} ${i8} ${i9}`)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return array;
    }
    isEqual(x1, ...x2) {
        return x2.some(item => item === x1);
    }
}
exports.FillArray104 = FillArray104;
// const fillArray104 = new FillArray104();
// console.log(
// 	fillArray104.fillArray()
// )
//# sourceMappingURL=FillArray104.js.map