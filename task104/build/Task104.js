"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FillArray104_1 = require("./FillArray104");
class Task104 extends FillArray104_1.FillArray104 {
    constructor() {
        //----------------------------------------
        super();
        this.combinationArray = [];
        this.combinationArray = this.fillArray();
        z1: for (const elem of this.combinationArray) {
            // console.log(elem.join(" "));
            const s1 = [elem[0], elem[1], elem[2], elem[3]];
            const s2 = [elem[4], elem[5], elem[6], elem[7]];
            const combo1 = this.returnCombo(s1);
            const combo2 = this.returnCombo(s2);
            for (const number1 of combo1) {
                for (const number2 of combo2) {
                    // console.log(number1, " ", number2)
                    if (number1 === number2) {
                        // [ 1, 2, 3, 9 ]   [ 4, 5, 8, 7 ]
                        // [ 132, 51, 240 ]   [ 465, 132, 591 ]
                        // 132  =  132
                        // 123 + 9 = 132     45 + 87 = 132
                        console.log(s1, " ", s2);
                        console.log(combo1, " ", combo2);
                        console.log(number1, " ", number2);
                        break z1;
                    }
                }
            }
        } //for
        //----------------------------------------
    }
    //############################################################################
    //############################################################################
    //############################################################################
    //############################################################################
    returnCombo(args) {
        const var1_1 = args[0] * 100 + args[1] * 10 + args[2];
        const var1_2 = args[3];
        const var2_1 = args[0] * 10 + args[1];
        const var2_2 = args[2] * 10 + args[3];
        const var3_1 = args[0];
        const var3_2 = args[1] * 100 + args[2] * 10 + args[3];
        return [
            var1_1 + var1_2,
            var2_1 + var2_2,
            var3_1 + var3_2,
        ];
    }
    pipe(...functions) {
        return function (item) {
            // console.log(">> ", item)
            let sum = 0;
            for (const function1 of functions) {
                const num = function1(item);
                // console.log("------> ", num);
                sum += num;
            }
            return sum;
            // return functions.reduce((output, func) => func(output), item);
        };
    }
    add(x) {
        return function (y) {
            // console.log("add >>> ", x, " + ", y)
            return x + y;
        };
    }
}
//##########################################################################
//##########################################################################
//##########################################################################
//##########################################################################
new Task104();
//# sourceMappingURL=Task104.js.map