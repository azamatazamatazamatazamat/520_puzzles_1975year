"use strict";
class Task444 {
    constructor() {
        this.fieldArray = [
            ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1', 'i1'],
            ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2', 'i2'],
            ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3', 'i3'],
            ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4', 'i4'],
            ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5', 'i5'],
            ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6', 'i6'],
            ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7', 'i7'],
            ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8', 'i8'],
            ['a9', 'b9', 'c9', 'd9', 'e9', 'f9', 'g9', 'h9', 'i9'],
        ];
        this.objFlies = {
            fly_1: {
                startingPosition: 'c1',
                currentPosition: '',
                wayPath: ['b1', 'd1', 'b2', 'c2', 'd2',]
            },
            fly_2: { startingPosition: 'f2',
                currentPosition: '',
                wayPath: ['e1', 'f1', 'g1', 'e2', 'g2', 'e3', 'f3', 'g3',] },
            fly_3: { startingPosition: 'i3',
                currentPosition: '',
                wayPath: ['h2', 'i2', 'h3', 'h4', 'i4',] },
            //-------------------------------------------------------------------------
            fly_4: { startingPosition: 'a4',
                currentPosition: '',
                wayPath: ['a3', 'b3', 'b4', 'a5', 'b5',] },
            fly_5: { startingPosition: 'h5',
                currentPosition: '',
                wayPath: ['g4', 'h4', 'i4', 'g5', 'i5', 'g6', 'h6', 'i6',] },
            fly_6: { startingPosition: 'd6',
                currentPosition: '',
                wayPath: ['c5', 'd5', 'e5', 'c6', 'e6', 'c7', 'd7', 'e7',] },
            //-------------------------------------------------------------------------
            fly_7: { startingPosition: 'b7',
                currentPosition: '',
                wayPath: ['a6', 'b6', 'c6', 'a7', 'c7', 'a8', 'b8', 'c8',] },
            fly_8: { startingPosition: 'g8',
                currentPosition: '',
                wayPath: ['f7', 'g7', 'h7', 'f8', 'h8', 'f9', 'g9', 'h9',] },
            fly_9: { startingPosition: 'e9',
                currentPosition: '',
                wayPath: ['d8', 'e8', 'f8', 'd9', 'f9',] },
        };
        //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    }
    display2() {
        const len = Object.entries(this.objFlies).length;
        // console.log(Object.keys(this.objFlies))
        // ['f1', 'f2', 'f3','f4', 'f5', 'f6','f7', 'f8', 'f9' ]
        const indexesFlag = Object.keys(this.objFlies).map((item, index) => {
            if (item.includes("f1") || item.includes("f2"))
                return -1;
            else
                return index;
        });
        console.log(indexesFlag);
        console.log(Object.entries(this.objFlies)[0]);
        console.log(Object.entries(this.objFlies)[0][1].wayPath);
        console.log(this.objFlies["fly_1"]);
        if (1)
            for (let i_1 = 0; i_1 < len; i_1++) {
                for (let i_2 = i_1 + 1; i_2 < len; i_2++) {
                    if (i_1 === i_2)
                        continue;
                    for (let i_3 = i_2 + 1; i_3 < len; i_3++) {
                        if (i_1 === i_3 || i_2 === i_3)
                            continue;
                        // const indexesFlag = Object.keys(this.objFlies).map((item,index)=>{
                        //     if(
                        //         item.includes( Object.entries(this.objFlies)[i_1][0] ) ||
                        //         item.includes( Object.entries(this.objFlies)[i_2][0] ) ||
                        //         item.includes( Object.entries(this.objFlies)[i_3][0] )
                        //     )
                        //         return -1;
                        //     else
                        //         return index;
                        // });
                        console.log(indexesFlag);
                        console.log(Object.entries(this.objFlies)[i_1][0] + "\t" +
                            Object.entries(this.objFlies)[i_2][0] + "\t" +
                            Object.entries(this.objFlies)[i_3][0] + "\t" +
                            "\n--------------------------------------------");
                    }
                }
            }
        console.log();
    }
    display() {
        const len = Object.entries(this.objFlies).length;
        z1: for (let i_1 = 0; i_1 < len; i_1++) {
            for (let i_2 = i_1 + 1; i_2 < len; i_2++) {
                if (i_1 === i_2)
                    continue;
                for (let i_3 = i_2 + 1; i_3 < len; i_3++) {
                    if (i_1 === i_3 || i_2 === i_3)
                        continue;
                    //-------------------------------------------------------------------------
                    Object.values(this.objFlies).forEach(item => {
                        item.currentPosition = item.startingPosition;
                    });
                    if (0)
                        Object.values(this.objFlies).forEach(item => {
                            console.log(item);
                        });
                    //-------------------------------------------------------------------------
                    const fly_x = Object.entries(this.objFlies)[i_1][0];
                    const fly_y = Object.entries(this.objFlies)[i_2][0];
                    const fly_z = Object.entries(this.objFlies)[i_3][0];
                    // console.log(fly_x + "\n" + fly_y + "\n" + fly_z + "\n");
                    const lenFly1 = this.objFlies[fly_x].wayPath.length;
                    const lenFly2 = this.objFlies[fly_y].wayPath.length;
                    const lenFly3 = this.objFlies[fly_z].wayPath.length;
                    for (let i1 = 0; i1 < lenFly1; i1++) {
                        for (let i2 = 0; i2 < lenFly2; i2++) {
                            for (let i3 = 0; i3 < lenFly3; i3++) {
                                const coordinationFly_x = this.objFlies[fly_x].wayPath[i1];
                                const coordinationFly_y = this.objFlies[fly_y].wayPath[i2];
                                const coordinationFly_z = this.objFlies[fly_z].wayPath[i3];
                                // console.log(coordinationFly_x+" "+coordinationFly_y+" "+coordinationFly_z)
                                this.objFlies[fly_x].currentPosition = coordinationFly_x;
                                this.objFlies[fly_y].currentPosition = coordinationFly_y;
                                this.objFlies[fly_z].currentPosition = coordinationFly_z;
                                // console.log( this.objFlies[fly_x].currentPosition );
                                // console.log( this.objFlies[fly_y].currentPosition );
                                // console.log( this.objFlies[fly_z].currentPosition );
                                if (this.horizontalCrossingFunc(len) || this.verticalCrossingFunc(len))
                                    continue;
                                if (this.displayField()) {
                                    Object.values(this.objFlies).forEach(item => {
                                        console.log((item.startingPosition === item.currentPosition) + "\t\t" + item.startingPosition + "\t\t" + item.currentPosition);
                                    });
                                    break z1;
                                }
                            }
                        }
                    }
                    //--------------------------------------------------------------------
                }
            }
        }
    }
    centralCrossingFunc(len) {
        for (let i1 = 0; i1 < len - 1; i1++) {
            const posit1 = Object.values(this.objFlies)[i1].currentPosition;
            for (let i2 = i1 + 1; i2 < len; i2++) {
                const posit2 = Object.values(this.objFlies)[i2].currentPosition;
                console.log(posit1 + "\t\t" + posit2);
                if (posit1 === posit2)
                    return true;
            }
        }
        return false;
    }
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    horizontalCrossingFunc(len) {
        for (let i1 = 0; i1 < len - 1; i1++) {
            const posit1 = Object.values(this.objFlies)[i1].currentPosition;
            for (let i2 = i1 + 1; i2 < len; i2++) {
                const posit2 = Object.values(this.objFlies)[i2].currentPosition;
                if (posit1[1] === posit2[1]) {
                    // console.log(posit1+"\t\t"+posit2)
                    return true;
                }
            }
        }
        return false;
    }
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    verticalCrossingFunc(len) {
        for (let i1 = 0; i1 < len - 1; i1++) {
            const posit1 = Object.values(this.objFlies)[i1].currentPosition;
            for (let i2 = i1 + 1; i2 < len; i2++) {
                const posit2 = Object.values(this.objFlies)[i2].currentPosition;
                if (posit1[0] === posit2[0]) {
                    // console.log(posit1+"\t\t"+posit2)
                    return true;
                }
            }
        }
        return false;
    }
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    displayField() {
        const innerArray = [
            [".", ".", ".", ".", ".", ".", ".", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", ".", "."],
            [".", ".", ".", ".", ".", ".", ".", ".", "."],
        ];
        Object.entries(this.objFlies).forEach(item => {
            const nameObj = item[0][item[0].length - 1];
            const position = item[1].currentPosition;
            // console.log(nameObj);
            // console.log(position);
            for (let i1 = 0; i1 < this.fieldArray.length; i1++) {
                for (let i2 = 0; i2 < this.fieldArray[i1].length; i2++) {
                    if (this.fieldArray[i1][i2] === position) {
                        innerArray[i1][i2] = nameObj;
                    }
                }
            }
        });
        // проверка на совпадение по диагоналям
        if (!this.solidus(innerArray) && !this.reverseSolidus(innerArray)) {
            innerArray.forEach(item => {
                const sumStr = item.reduce((sum, current) => sum += (current + "  "), "");
                console.log(sumStr);
            });
            return true;
        }
        return false;
    }
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    solidus(innerArray) {
        const concatArray = [];
        concatArray.push(innerArray[0]);
        concatArray.push(['.'].concat(innerArray[1]));
        concatArray.push(['.', '.'].concat(innerArray[2]));
        concatArray.push(['.', '.', '.'].concat(innerArray[3]));
        concatArray.push(['.', '.', '.', '.'].concat(innerArray[4]));
        concatArray.push(['.', '.', '.', '.', '.'].concat(innerArray[5]));
        concatArray.push(['.', '.', '.', '.', '.', '.'].concat(innerArray[6]));
        concatArray.push(['.', '.', '.', '.', '.', '.', '.'].concat(innerArray[7]));
        concatArray.push(['.', '.', '.', '.', '.', '.', '.', '.'].concat(innerArray[8]));
        if (0)
            concatArray.forEach(item => {
                // console.log(item+"  ")
                const sumStr = item.reduce((sum, current) => sum += (current + "  "), "");
                console.log(sumStr);
            });
        // .  .  .  .  .  2  .  .  .
        // .  .  .  .  1  .  .  .  .  .
        // .  .  .  .  .  .  .  .  .  .  3
        // .  .  .  4  .  .  .  .  .  .  .  .
        // .  .  .  .  .  .  .  .  .  .  .  5  .
        // .  .  .  .  .  .  .  6  .  .  .  .  .  .
        // .  .  .  .  .  .  .  7  .  .  .  .  .  .  .
        // .  .  .  .  .  .  .  .  .  .  .  .  .  8  .  .
        // .  .  .  .  .  .  .  .  .  .  .  .  9  .  .  .  .
        let count = 0;
        let isCount = 0;
        z1: while (true) {
            isCount = 0;
            for (let i1 = concatArray.length - 1; i1 >= 0; i1--) {
                if (concatArray[i1][count] === undefined) {
                    count++;
                    // console.log("-----")
                    continue z1;
                }
                if (concatArray[i1][count] !== '.')
                    isCount++;
                // console.log(concatArray[i1][count]+"\t"+isCount)
                if (isCount > 1)
                    return true;
                if (0 === i1) {
                    count++;
                    // console.log("-----")
                    continue z1;
                }
                if (concatArray[concatArray.length - 1].length - 1 === count)
                    break z1;
            }
        }
        return false;
    }
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    reverseSolidus(innerArray) {
        const concatArray = [];
        concatArray.push(['.', '.', '.', '.', '.', '.', '.', '.'].concat(innerArray[0]));
        concatArray.push(['.', '.', '.', '.', '.', '.', '.'].concat(innerArray[1]));
        concatArray.push(['.', '.', '.', '.', '.', '.'].concat(innerArray[2]));
        concatArray.push(['.', '.', '.', '.', '.'].concat(innerArray[3]));
        concatArray.push(['.', '.', '.', '.'].concat(innerArray[4]));
        concatArray.push(['.', '.', '.'].concat(innerArray[5]));
        concatArray.push(['.', '.'].concat(innerArray[6]));
        concatArray.push(['.'].concat(innerArray[7]));
        concatArray.push(innerArray[8]);
        if (0)
            concatArray.forEach(item => {
                // console.log(item+"  ")
                const sumStr = item.reduce((sum, current) => sum += (current + "  "), "");
                console.log(sumStr);
            });
        // .  .  .  .  .  .  .  .  .  .  .  .  .  2  .  .  .
        // .  .  .  .  .  .  .  .  .  .  1  .  .  .  .  .
        // .  .  .  .  .  .  .  .  .  .  .  .  .  .  3
        // .  .  .  .  .  4  .  .  .  .  .  .  .  .
        // .  .  .  .  .  .  .  .  .  .  .  5  .
        // .  .  .  .  .  6  .  .  .  .  .  .
        // .  .  .  7  .  .  .  .  .  .  .
        // .  .  .  .  .  .  .  8  .  .
        // .  .  .  .  9  .  .  .  .
        let count = 0;
        let isCount = 0;
        z1: while (true) {
            isCount = 0;
            for (let i1 = 0; i1 < concatArray.length; i1++) {
                if (concatArray[i1][count] === undefined) {
                    count++;
                    // console.log("-----")
                    continue z1;
                }
                if (concatArray[i1][count] !== '.')
                    isCount++;
                // console.log(concatArray[i1][count]+"\t"+isCount)
                if (isCount > 1)
                    return true;
                if (concatArray.length - 1 === i1) {
                    count++;
                    // console.log("-----")
                    continue z1;
                }
                if (concatArray[0].length - 1 === count)
                    break z1;
            }
        }
        return false;
    }
}
//############################################################################
//############################################################################
//############################################################################
//############################################################################
let task439 = new Task444();
task439.display();
// task439.display2()
//# sourceMappingURL=Task444.js.map