import {FillArray} from "./FillArray";
interface ISudoku {
    array:number[][];
    referenceArray:number[][];
    len: number;
    bigNum: number;
}

class Sudoku extends FillArray implements ISudoku{
    array:number[][] = [];
    referenceArray:number[][] = [];
    len: number;
    bigNum: number = 0;

    constructor(len: number, referenceArray: number[][]){
        super();
        this.len = len;
        this.referenceArray = referenceArray;
    }

    mainFunction(){
        this.bigNum = this.biggerNumberInArray(this.referenceArray);
        console.log(this.bigNum)
        this.fillArray(this.array,this.len)
        // this.display(this.referenceArray)

        if(1)
        this.array.some(item1 => {
            if(this.firstBarrier(item1, this.referenceArray,this.bigNum)){
            // if(1)
            // z1:
            for(let i1:number = 0; i1 < this.array.length; i1++){
                if(item1[0] !== this.array[i1][this.bigNum]) continue;
                // console.log(this.array[i1]," <");
                if(!this.secondBarrier(this.bigNum,this.array,this.referenceArray,i1,0)) continue;

            for(let i2:number = 0; i2 < this.array.length; i2++){
                if(item1[1] !== this.array[i2][this.bigNum]) continue;
                // console.log(this.array[i2]," <<");
                if(!this.secondBarrier(this.bigNum,this.array,this.referenceArray,i2,1)) continue;
                if(this.thirdBarrier(this.bigNum, this.array, 1, i1,i2)) continue;
                if(this.fourthBarrier(this.array, i1,i2)) continue;

            for(let i3:number = 0; i3 < this.array.length; i3++){
                if(item1[2] !== this.array[i3][this.bigNum]) continue;
                // console.log(this.array[i3]," <<<");
                if(!this.secondBarrier(this.bigNum,this.array,this.referenceArray,i3,2)) continue;
                if(this.thirdBarrier(this.bigNum, this.array, 2, i1,i2,i3)) continue;

            for(let i4:number = 0; i4 < this.array.length; i4++){
                if(item1[3] !== this.array[i4][this.bigNum]) continue;
                // console.log(this.array[i4]," <<<<");
                if(!this.secondBarrier(this.bigNum,this.array,this.referenceArray,i4,3)) continue;
                if(this.thirdBarrier(this.bigNum, this.array, 3, i1,i2,i3,i4)) continue;
                if(this.fourthBarrier(this.array, i3,i4)) continue;

            for(let i5:number = 0; i5 < this.array.length; i5++){
                if(item1[4] !== this.array[i5][this.bigNum]) continue;
                // console.log(this.array[i5]," <<<<<");
                if(!this.secondBarrier(this.bigNum,this.array,this.referenceArray,i5,4)) continue;
                if(this.thirdBarrier(this.bigNum, this.array,4,  i1,i2,i3,i4,i5)) continue;

            for(let i6:number = 0; i6 < this.array.length; i6++){
                if(item1[5] !== this.array[i6][this.bigNum]) continue;
                // console.log(this.array[i6]," <<<<<<");
                if(!this.secondBarrier(this.bigNum,this.array,this.referenceArray,i6,5)) continue;
                if(this.thirdBarrier(this.bigNum, this.array,5,  i1,i2,i3,i4,i5,i6)) continue;
                if(this.fourthBarrier(this.array, i5,i6)) continue;

                console.log(this.array[i1])
                console.log(this.array[i2])
                console.log(this.array[i3])
                console.log(this.array[i4])
                console.log(this.array[i5])
                console.log(this.array[i6])

                return true
                // break z1
            }}}}}}
            // return true
            }
        })
    }
//#################################################################################################
    display(array:number[][]){
        array.some(item => {
            console.log(item)
        })
        // console.log(`${array[0]} ${array[1]} ${array[2]} ${array[3]} ${array[4]} ${array[5]}`)
    }

    private firstBarrier(item: number[], referenceArray: number[][], index: number){
        if(item[0] === referenceArray[0][index] || referenceArray[0][index] === 0)
        if(item[1] === referenceArray[1][index] || referenceArray[1][index] === 0)
        if(item[2] === referenceArray[2][index] || referenceArray[2][index] === 0)
        if(item[3] === referenceArray[3][index] || referenceArray[3][index] === 0)
        if(item[4] === referenceArray[4][index] || referenceArray[4][index] === 0)
        if(item[5] === referenceArray[5][index] || referenceArray[5][index] === 0)
            return true;
    }

    private secondBarrier(
        bigNum:number,
        array:number[][],
        referenceArray:number[][],
        index:number,
        referIndex:number
    ):boolean{
        const isFlag0:boolean = (bigNum === 0) ? true : (referenceArray[referIndex][0] === 0 || referenceArray[referIndex][0] === array[index][0]);
        const isFlag1:boolean = (bigNum === 1) ? true : (referenceArray[referIndex][1] === 0 || referenceArray[referIndex][1] === array[index][1]);
        const isFlag2:boolean = (bigNum === 2) ? true : (referenceArray[referIndex][2] === 0 || referenceArray[referIndex][2] === array[index][2]);
        const isFlag3:boolean = (bigNum === 3) ? true : (referenceArray[referIndex][3] === 0 || referenceArray[referIndex][3] === array[index][3]);
        const isFlag4:boolean = (bigNum === 4) ? true : (referenceArray[referIndex][4] === 0 || referenceArray[referIndex][4] === array[index][4]);
        const isFlag5:boolean = (bigNum === 5) ? true : (referenceArray[referIndex][5] === 0 || referenceArray[referIndex][5] === array[index][5]);

        if(isFlag0 && isFlag1 && isFlag2 && isFlag3 && isFlag4 && isFlag5)
            return true;
        else
            return false;
    }

    private thirdBarrier(bigNum:number, array:number[][], nestingLevel:number, ...VZ:number[]){
        // const isFlag0:boolean = (0 === bigNum) ? false : true;
        const isFlag0:boolean = (0 !== bigNum);
        const isFlag1:boolean = (1 !== bigNum);
        const isFlag2:boolean = (2 !== bigNum);
        const isFlag3:boolean = (3 !== bigNum);
        const isFlag4:boolean = (4 !== bigNum);
        const isFlag5:boolean = (5 !== bigNum);

        if(isFlag0 && this.secondBarrierNestedFunc(0, array, nestingLevel,...VZ))return true;
        if(isFlag1 && this.secondBarrierNestedFunc(1, array, nestingLevel,...VZ))return true;
        if(isFlag2 && this.secondBarrierNestedFunc(2, array, nestingLevel,...VZ))return true;
        if(isFlag3 && this.secondBarrierNestedFunc(3, array, nestingLevel,...VZ))return true;
        if(isFlag4 && this.secondBarrierNestedFunc(4, array, nestingLevel,...VZ))return true;
        if(isFlag5 && this.secondBarrierNestedFunc(5, array, nestingLevel,...VZ))return true;
    }

// HI = horizontalIndexes
// VZ = verticalIndexes
    private secondBarrierNestedFunc(HI:number, array:number[][], nestingLevel:number, ...VZ:number[]){
        // console.log(indexes)
        switch(nestingLevel){
            case 1:
                if( array[VZ[0]][HI] === array[VZ[1]][HI]) {
                    return true;
                }else
                    return false;
            case 2:
                if( array[VZ[0]][HI] === array[VZ[2]][HI] ||
                    array[VZ[1]][HI] === array[VZ[2]][HI]) {
                    return true;
                }else
                    return false;
            case 3:
                if( array[VZ[0]][HI] === array[VZ[3]][HI] ||
                    array[VZ[1]][HI] === array[VZ[3]][HI] ||
                    array[VZ[2]][HI] === array[VZ[3]][HI]) {
                    return true;
                }else
                    return false;
            case 4:
                if( array[VZ[0]][HI] === array[VZ[4]][HI] ||
                    array[VZ[1]][HI] === array[VZ[4]][HI] ||
                    array[VZ[2]][HI] === array[VZ[4]][HI] ||
                    array[VZ[3]][HI] === array[VZ[4]][HI]) {
                    return true;
                }else
                    return false;
            case 5:
                if( array[VZ[0]][HI] === array[VZ[5]][HI] ||
                    array[VZ[1]][HI] === array[VZ[5]][HI] ||
                    array[VZ[2]][HI] === array[VZ[5]][HI] ||
                    array[VZ[3]][HI] === array[VZ[5]][HI] ||
                    array[VZ[4]][HI] === array[VZ[5]][HI]) {
                    return true;
                }else
                    return false;
            default:
                return true;
        }
    }

    private fourthBarrier(array:number[][],...VZ:number[]){
        if( array[VZ[0]][0] === array[VZ[1]][0] ||
            array[VZ[0]][0] === array[VZ[1]][1] ||
            array[VZ[0]][0] === array[VZ[1]][2] ||
            array[VZ[0]][1] === array[VZ[1]][0] ||
            array[VZ[0]][1] === array[VZ[1]][1] ||
            array[VZ[0]][1] === array[VZ[1]][2] ||
            array[VZ[0]][2] === array[VZ[1]][0] ||
            array[VZ[0]][2] === array[VZ[1]][1] ||
            array[VZ[0]][2] === array[VZ[1]][2] ||

            array[VZ[0]][3] === array[VZ[1]][3] ||
            array[VZ[0]][3] === array[VZ[1]][4] ||
            array[VZ[0]][3] === array[VZ[1]][5] ||
            array[VZ[0]][4] === array[VZ[1]][3] ||
            array[VZ[0]][4] === array[VZ[1]][4] ||
            array[VZ[0]][4] === array[VZ[1]][5] ||
            array[VZ[0]][5] === array[VZ[1]][3] ||
            array[VZ[0]][5] === array[VZ[1]][4] ||
            array[VZ[0]][5] === array[VZ[1]][5] ) return true;
    }

    private biggerNumberInArray(referenceArray:number[][]){
        if(0)
            referenceArray.forEach((item:number[])=>{
                console.log(item[0]," ",item[1]," ",item[2]," ",item[3]," ",item[4]," ",item[5])
            })

        let innerArray:number[] = [0,0,0,0,0,0];
        const array:number[] = referenceArray.reduce((sum,item)=>{
            if(item[0] !== 0) innerArray[0]++;
            if(item[1] !== 0) innerArray[1]++;
            if(item[2] !== 0) innerArray[2]++;
            if(item[3] !== 0) innerArray[3]++;
            if(item[4] !== 0) innerArray[4]++;
            if(item[5] !== 0) innerArray[5]++;

            sum = innerArray;
            return sum;
        }, [])

        const innerBigNum:number = Math.max.apply(Math, array.map(item => item)); // 4 самое большое число в массиве
        // console.log(array.findIndex(item => item === biggerNum));// 0 это первый индекс с большим числом в массиве
        return array.findIndex(item => item === innerBigNum);
    }
}

const globalArray:number[][] =[
    // [2, 6, 0, 5, 0, 1],
    // [0, 0, 1, 0, 0, 2],
    // [4, 0, 0, 0, 6, 0],
    // [0, 3, 0, 0, 0, 4],
    // [1, 0, 0, 4, 0, 0],
    // [3, 0, 4, 0, 2, 6],

    // [0, 0, 0, 3, 0, 0], //[ 4, 2, 1, 3, 5, 6 ]
    // [0, 0, 5, 4, 0, 2], //[ 6, 3, 5, 4, 1, 2 ]
    // [2, 1, 0, 0, 6, 0], //[ 2, 1, 3, 5, 6, 4 ]
    // [0, 4, 0, 0, 2, 3], //[ 5, 4, 6, 1, 2, 3 ]
    // [3, 0, 2, 6, 0, 0], //[ 3, 5, 2, 6, 4, 1 ]
    // [0, 0, 4, 0, 0, 0], //[ 1, 6, 4, 2, 3, 5 ]

    [6, 0, 0, 5, 1, 0], //[ 6, 2, 4, 5, 1, 3 ]
    [3, 0, 0, 0, 0, 0], //[ 3, 5, 1, 6, 4, 2 ]
    [0, 0, 5, 0, 0, 0], //[ 2, 4, 5, 3, 6, 1 ]
    [0, 0, 0, 4, 0, 0], //[ 1, 3, 6, 4, 2, 5 ]
    [0, 0, 0, 0, 0, 4], //[ 5, 6, 2, 1, 3, 4 ]
    [0, 1, 3, 0, 0, 6], //[ 4, 1, 3, 2, 5, 6 ]

]

// let fillArray = new fillArray();
let sudoku = new Sudoku(6,globalArray);
sudoku.mainFunction()