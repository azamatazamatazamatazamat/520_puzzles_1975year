// const pair = (...args) => Object.freeze( args.slice(0,2) );

let user: [string, number] = ["Tom", 36];
let array:any[]=[];
array.push(user);
console.log(array)



// let user: [string, number] = ["Tom", 36];
// for(const prop of user){
//     console.log(prop);
// }

// console.log(user[1]); // 36
// user[1] = 37;
// console.log(user[1]); // 37

interface MyTuple extends ReadonlyArray<string|number> {
    0: string;
    1: number;
    length: 2;
}

const tup: MyTuple = <MyTuple>Object.freeze(['foo', 123]);
console.log(tup)
// или
// const tup: MyTuple = Object.freeze(['foo', 123]) as MyTuple;
// // или
// const tup: MyTuple = ['foo', 123]; Object.freeze(tup);