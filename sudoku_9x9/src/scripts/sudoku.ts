import {findEmptyCell} from "./findEmptyCell";
import {isValid} from "./isValid";
import {display} from "./display";

function solveSudoku(board: string[][]): boolean{
	const emptyCell = findEmptyCell(board);
	console.log(emptyCell)
	console.log("start");
	if(!emptyCell){
		console.log(emptyCell)
		return true; // Если все клетки заполнены, судоку решено
	}
	const [row, col] = emptyCell;
	for(let i1 = 1; i1 <= 9; i1++){
		const numStr = i1.toString();
		console.log(">> ",i1," <<");
		console.log(display(board, row, col));

		if(isValid(board, row, col, numStr)){
			board[row][col] = numStr; // Помещаем число в клетку
			if(solveSudoku(board)){
				console.log("return true");
				return true;
			}else
				board[row][col] = '.'; // Если не нашли правильное число, возвращаем клетку к пустому значению и пробуем другие варианты
		}
	}
	console.log("stop");
	console.log("return false");
	return false; // Если не существует правильного числа для данной клетки, возвращаемся назад
}

// Пример заполнения судоку (точки представляют пустые клетки)
const sudokuBoard: string[][] = [
	['.', '3', '.', '.', '7', '.', '.', '.', '.'],
	['6', '.', '.', '1', '9', '5', '.', '.', '.'],
	['.', '9', '8', '.', '.', '.', '.', '6', '.'],
	['8', '.', '.', '.', '6', '.', '.', '.', '3'],
	['4', '.', '.', '8', '.', '3', '.', '.', '1'],
	['7', '.', '.', '.', '2', '.', '.', '.', '6'],
	['.', '6', '.', '.', '.', '.', '2', '8', '.'],
	['.', '.', '.', '4', '1', '9', '.', '.', '5'],
	['.', '.', '.', '.', '8', '.', '.', '7', '9']
];


if(solveSudoku(sudokuBoard)){
	console.log('Решенное судоку:');
	console.table(sudokuBoard);
}else{
	console.log('Нет решения для данного судоку');
}
