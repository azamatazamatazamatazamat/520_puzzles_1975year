// const sudokuBoard2: string[][] = [
// 	['.', '3', '.', '.', '7', '.', '.', '.', '.'],
// 	['6', '.', '.', '1', '9', '5', '.', '.', '.'],
// 	['.', '9', '8', '.', '.', '.', '.', '6', '.'],
// 	['8', '.', '.', '.', '6', '.', '.', '.', '3'],
// 	['4', '.', '.', '8', '.', '3', '.', '.', '1'],
// 	['7', '.', '.', '.', '2', '.', '.', '.', '6'],
// 	['.', '6', '.', '.', '.', '.', '2', '8', '.'],
// 	['.', '.', '.', '4', '1', '9', '.', '.', '5'],
// 	['.', '.', '.', '.', '8', '.', '.', '7', '9']
// ];
//
// console.log(
// display(sudokuBoard2, 1, 2)
// )

export function display(board: string[][], row: number, col: number){
	let outerStr = "";
	board.forEach((item, index)=>{
		let innerStr = ""
		item.forEach((item2, index2)=>{
			if(index === row && index2 === col)
				innerStr += `<${item2}> `
			else
				innerStr += ` ${item2}  `
		});
		outerStr += (innerStr+"\n")
	});
	return outerStr;
}