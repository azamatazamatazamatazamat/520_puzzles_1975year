import {display} from "./display";

const scanBlock = (row: number, col: number, i2: number)=>{
	const i_1 = 3 * Math.floor(row / 3) + Math.floor(i2 / 3);
	const i_2 = 3 * Math.floor(col / 3) + (i2 % 3);
	return [i_1, i_2];
}

type TInnerDisplay = {
	i_1: number;
	i_2: number;
	i1: string;
	i2: number;
	row: number;
	col: number;
	board: string[][];
};

const innerDisplay = ({i_1, i_2, i1, i2, row, col, board}: TInnerDisplay)=>{
	// console.log(board)
	console.log("-------------------------------------------------------")
	console.log(display(board, row, col));
	console.log("row=", row, "\t| col=", col);
	console.log("i_1 = ", i_1);
	console.log("i_2 = ", i_2);
	console.log("board[row][i2] = ", board[row][i2], "\t| row =", row, "\t| i2 =", i2, "| horizontal");
	console.log("board[i2][col] = ", board[i2][col], "\t| i2 =", i2, "\t| col =", col, "| vertical");
	console.log("board[ss1][ss2] = ", board[i_1][i_2], "\t| ss1 =", i_1, "\t| ss2 =", i_2, "| block");
	console.log("i1\t\t\t\t<", i1, ">");
	console.log("i2\t\t\t\t<", i2, ">");
}


export function isValid(board: string[][], row: number, col: number, i1: string): boolean{
	for(let i2 = 0; i2 < 9; i2++){
		const [i_1, i_2] = scanBlock(row, col, i2)
		const is1 = board[row][i2] === i1;
		const is2 = board[i2][col] === i1;
		const is3 = board[i_1][i_2] === i1;
		// innerDisplay({i_1, i_2, i1, i2, row, col, board});
		if(is1 || is2 || is3){
			return false; // Проверяем, можно ли поместить число в данную клетку
		}
	}
	return true;
}