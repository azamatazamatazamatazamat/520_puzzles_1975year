export function findEmptyCell(board: string[][]): [number, number] | null{
	for(let row = 0; row < 9; row++){
		for(let col = 0; col < 9; col++){
			if(board[row][col] === '.'){
				return [row, col]; // Нашли пустую клетку
			}
		}
	}
	return null; // Не осталось пустых клеток, судоку решено
}