type TObj=NonNullable<{
	[block: string]: {
		// put: boolean,
		// took: boolean,
		arr: number[],
	}
}>

const copyValue=(myObject: TObj, fromKey: string, toKey: string)=>{
	const value=myObject[fromKey].arr.pop();
	myObject[toKey].arr.push(value!);
}

const compareValue=(myObject: TObj, fromKey: string, toKey: string)=>{
	if (!myObject[fromKey].arr.length)// если пусто из копируемого массива => continue
		return true;
	if (!myObject[toKey].arr.length)//  если пусто куда копируем
		return false;
	else {
		const lenFromKey=myObject[fromKey].arr.at(-1);
		const lenToKey=myObject[toKey].arr.at(-1);
		if (lenFromKey! > lenToKey!)//   если куда копируем больше, чем откуда копируем
			return true;
		else
			return false;
	}
};

const compareObjects=()=>{

}

// const flagTookPut=(myObject: TObj, fromKey: string, toKey: string)=>{
// 	for (const myObjectKey in myObject) {
// 		// myObject[myObjectKey].took=false;
// 		myObject[myObjectKey].put=false;
// 	}
// 	// myObject[fromKey].took=true;
// 	myObject[toKey].put=true;
// };

const arrBlock=["block1", "block2", "block3"];
const position=[
	[0, 1],
	[0, 2],
	[1, 0],
	[1, 2],
	[2, 0],
	[2, 1],
]

const main_2=()=>{
	const protocolObj: string[]=[];
	const arrayNumberEquation=(myObject: TObj)=>{
		console.log("start")
		if (myObject.block3.arr.length === 5)
			return true;
		
		console.log(JSON.stringify(myObject, null, 2));
		do {
//################################################################
			for (let i=0; i < position.length; i++) {
				const [i1, i2]=position[i];
				
				if (compareValue(myObject, arrBlock[i1], arrBlock[i2])) continue;
				copyValue(myObject, arrBlock[i1], arrBlock[i2]);
				if (protocolObj.some(item=>item === JSON.stringify(myObject))) {
					copyValue(myObject, arrBlock[i2], arrBlock[i1]);
					console.log("************************************")
					console.log(JSON.stringify(myObject, null, 2));
					continue;
				}
				
				console.log("----------------------------------")
				console.log(JSON.stringify(myObject, null, 2));
				console.log(">>> ",JSON.stringify(protocolObj,null,2));
				
				protocolObj.push(JSON.stringify(myObject))
				
				arrayNumberEquation(myObject);
				// if (arrayNumberEquation(myObject)) {
				// 	return true;
				// } else {
				// 	console.log(">>> ", JSON.stringify(myObject, null, 2));
				// 	console.log(i1, ">>> ", i2);
				// }
				
				
			}
//################################################################
			console.log("end")
			console.log(">>> ", JSON.stringify(protocolObj, null, 2));
			console.log(protocolObj.length)
			console.log(Array.from(new Set(protocolObj)).length)
			if (myObject.block3.arr.length === 5)
				break;
			
		} while (true);
		
		return false;
	};
//###########################################################################################################
	const neeObj: TObj={
		block1: {
			arr: [5, 4, 3, 2, 1],
		},
		block2: {
			arr: [],
		},
		block3: {
			arr: [],
		}
	}
	arrayNumberEquation(neeObj);
}
main_2()
