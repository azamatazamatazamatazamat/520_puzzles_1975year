"use strict";
// 111
// 333
// 555
// 777
// 999
//ответ  100 330 505 077 099
class Task232 {
    constructor() {
        this.totalArray = [];
    }
    display() {
        const innerArray = this.fillArray(this.totalArray);
        const len = innerArray[0].length;
        z1: for (let i1 = 0; i1 < len; i1++) {
            for (let i2 = 0; i2 < len; i2++) {
                for (let i3 = 0; i3 < len; i3++) {
                    for (let i4 = 0; i4 < len; i4++) {
                        for (let i5 = 0; i5 < len; i5++) {
                            if (innerArray[0][i1] +
                                innerArray[1][i2] +
                                innerArray[2][i3] +
                                innerArray[3][i4] +
                                innerArray[4][i5] === 1111) {
                                const innerStr = this.addNull(String(innerArray[0][i1])) + ' ' +
                                    this.addNull(String(innerArray[1][i2])) + ' ' +
                                    this.addNull(String(innerArray[2][i3])) + ' ' +
                                    this.addNull(String(innerArray[3][i4])) + ' ' +
                                    this.addNull(String(innerArray[4][i5]));
                                if (this.countNull(innerStr) === 6)
                                    console.log(innerStr); // 100 330 505 077 099
                            }
                        }
                    }
                }
            }
        }
    }
    //######################################################################
    //######################################################################
    //######################################################################
    //######################################################################
    addNull(str) {
        switch (str.length) {
            case 1: return '00' + str;
            case 2: return '0' + str;
            default: return str;
        }
    }
    countNull(innerStr) {
        return Array.from(innerStr).reduce((sum, item) => {
            if (item === '0')
                sum += 1;
            return sum;
        }, 0);
    }
    fillArray(totalArray) {
        const innerNumsArray = ['111', '333', '555', '777', '999'];
        const innerNumsVariationArray = ['_23456789', '12_456789', '1234_6789', '123456_89', '12345678_'];
        innerNumsArray.forEach((item, index) => {
            const innerArray = [];
            for (let i1 = 0; i1 <= Number(item); i1++) {
                let numToStr = String(i1);
                if (this.isContainNum(numToStr, innerNumsVariationArray[index]))
                    continue;
                innerArray.push(i1);
                // console.log(i1)
            }
            totalArray.push([...innerArray]);
        });
        // console.log(totalArray)
        return totalArray;
    }
    isContainNum(toStr, numsStr) {
        let innerArray = numsStr.split('');
        return innerArray.some(item => {
            if (toStr.includes(item))
                return true;
        });
    }
}
let task232 = new Task232();
task232.display();
//# sourceMappingURL=Task232.js.map