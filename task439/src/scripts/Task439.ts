interface IPackageUser {
    playersCombinationsArray:string[],
}

class Task439 implements IPackageUser{
    playersCombinationsArray:string[] = ['A','B','C','D','E','F',];

    public display():void{
        const innerArrayCombo:string[] = this.playersCombinationsFunc(this.playersCombinationsArray);
        // console.log(innerArrayCombo)
        // [
        //     'AB', 'AC', 'AD', 'AE',
        //     'AF', 'BC', 'BD', 'BE',
        //     'BF', 'CD', 'CE', 'CF',
        //     'DE', 'DF', 'EF'
        // ]

        const innerArrayTwoDim:string[][] = this.partyPlayersFunc(innerArrayCombo);
        // console.log(innerArrayTwoDim)

        const len:number = innerArrayTwoDim.length;

        if(1)
        z1:
        for(let i1 = 0; i1 < len; i1++){
        for(let i2 = 0; i2 < len; i2++){
            if( this.filterFunc(innerArrayTwoDim,i1,i2) )continue;

        for(let i3 = 0; i3 < len; i3++){
            if( this.filterFunc(innerArrayTwoDim,i1,i3) ||
                this.filterFunc(innerArrayTwoDim,i2,i3) )continue;

            if( this.findAllDuplicate(innerArrayTwoDim,0,i1,i2,i3) ||
                this.findAllDuplicate(innerArrayTwoDim,1,i1,i2,i3) ||
                this.findAllDuplicate(innerArrayTwoDim,2,i1,i2,i3) )continue;

        for(let i4 = 0; i4 < len; i4++){
            if( this.filterFunc(innerArrayTwoDim,i1,i4) ||
                this.filterFunc(innerArrayTwoDim,i2,i4) ||
                this.filterFunc(innerArrayTwoDim,i3,i4) )continue;

            if( this.findAllDuplicate(innerArrayTwoDim,0,i1,i2,i3,i4) ||
                this.findAllDuplicate(innerArrayTwoDim,1,i1,i2,i3,i4) ||
                this.findAllDuplicate(innerArrayTwoDim,2,i1,i2,i3,i4) )continue;

        for(let i5 = 0; i5 < len; i5++){
            if( this.filterFunc(innerArrayTwoDim,i1,i5) ||
                this.filterFunc(innerArrayTwoDim,i2,i5) ||
                this.filterFunc(innerArrayTwoDim,i3,i5) ||
                this.filterFunc(innerArrayTwoDim,i4,i5) )continue;

            if( this.findAllDuplicate(innerArrayTwoDim,0,i1,i2,i3,i4,i5) ||
                this.findAllDuplicate(innerArrayTwoDim,1,i1,i2,i3,i4,i5) ||
                this.findAllDuplicate(innerArrayTwoDim,2,i1,i2,i3,i4,i5) )continue;

                console.log(
innerArrayTwoDim[i1][0]+"\t"+innerArrayTwoDim[i1][1]+"\t"+innerArrayTwoDim[i1][2]+"\n"+
innerArrayTwoDim[i2][0]+"\t"+innerArrayTwoDim[i2][1]+"\t"+innerArrayTwoDim[i2][2]+"\n"+
innerArrayTwoDim[i3][0]+"\t"+innerArrayTwoDim[i3][1]+"\t"+innerArrayTwoDim[i3][2]+"\n"+
innerArrayTwoDim[i4][0]+"\t"+innerArrayTwoDim[i4][1]+"\t"+innerArrayTwoDim[i4][2]+"\n"+
innerArrayTwoDim[i5][0]+"\t"+innerArrayTwoDim[i5][1]+"\t"+innerArrayTwoDim[i5][2]+"\n"+
                    "------------------"
                );
                break z1
            // AB	CD	EF
            // AC	BE	DF
            // BF	CE	AD
            // CF	BD	AE
            // DE	AF	BC
        }}}}}
    }

    // private findAllDuplicate(innerArrayTwoDim:string[][],...indexArray:number[]){
    private findAllDuplicate(innerArrayTwoDim:string[][],innerIndex:number,...indexArray:number[]):boolean{
        let concatStr:string = "";
        for(const number of indexArray){
            concatStr += innerArrayTwoDim[ number ][innerIndex];
        }
        // console.log(indexArray[0])
        // console.log(indexArray[1])
        // console.log(indexArray[2])

        // console.log(concatStr) // ABACAD

        // let trigger = "AEACCF";
        // console.log(Object(concatStr.match(/A/g)).length > 2);
        // playersCombinationsArray:string[] = ['A','B','C','D','E','F',];

        if(
            Object(concatStr.match(/A/g)).length > 2 ||
            Object(concatStr.match(/B/g)).length > 2 ||
            Object(concatStr.match(/C/g)).length > 2 ||
            Object(concatStr.match(/D/g)).length > 2 ||
            Object(concatStr.match(/E/g)).length > 2 ||
            Object(concatStr.match(/F/g)).length > 2
        )return true;
        return false
    }

    private filterFunc(innerArrayTwoDim:string[][], ...arrIndexes:number[]):boolean{
        if(
            innerArrayTwoDim[ arrIndexes[0] ][0] === innerArrayTwoDim[ arrIndexes[1] ][0] ||
            innerArrayTwoDim[ arrIndexes[0] ][0] === innerArrayTwoDim[ arrIndexes[1] ][1] ||
            innerArrayTwoDim[ arrIndexes[0] ][0] === innerArrayTwoDim[ arrIndexes[1] ][2] ||

            innerArrayTwoDim[ arrIndexes[0] ][1] === innerArrayTwoDim[ arrIndexes[1] ][0] ||
            innerArrayTwoDim[ arrIndexes[0] ][1] === innerArrayTwoDim[ arrIndexes[1] ][1] ||
            innerArrayTwoDim[ arrIndexes[0] ][1] === innerArrayTwoDim[ arrIndexes[1] ][2] ||

            innerArrayTwoDim[ arrIndexes[0] ][2] === innerArrayTwoDim[ arrIndexes[1] ][0] ||
            innerArrayTwoDim[ arrIndexes[0] ][2] === innerArrayTwoDim[ arrIndexes[1] ][1] ||
            innerArrayTwoDim[ arrIndexes[0] ][2] === innerArrayTwoDim[ arrIndexes[1] ][2]
        ) return true;
        return false;
    }


    private playersCombinationsFunc(playersCombinationsArray:string[]):string[]{
        const innerArrayCombo:string[] = [];
        const len:number = playersCombinationsArray.length
        for(let i1 = 0; i1 < len; i1++){
        for(let i2 = i1+1; i2 < len; i2++){
            if(playersCombinationsArray[i1]===playersCombinationsArray[i2]) continue
            innerArrayCombo.push(playersCombinationsArray[i1]+""+playersCombinationsArray[i2])
            // console.log(innerArray[i1]+""+innerArray[i2])
        }}
        return innerArrayCombo;
    }

    private partyPlayersFunc(arrayCombo:string[]):string[][]{
        const len:number = arrayCombo.length
        const innerArrayTwoDim:string[][] = [];

        for(let i1 = 0; i1 < len; i1++){
        for(let i2 = 0; i2 < len; i2++){
            if(
                arrayCombo[i1].includes(arrayCombo[i2][0]) ||
                arrayCombo[i1].includes(arrayCombo[i2][1])
            )continue

        for(let i3 = 0; i3 < len; i3++){
            if(
                arrayCombo[i1].includes(arrayCombo[i3][0]) ||
                arrayCombo[i1].includes(arrayCombo[i3][1]) ||
                arrayCombo[i2].includes(arrayCombo[i3][0]) ||
                arrayCombo[i2].includes(arrayCombo[i3][1])
            )continue

            innerArrayTwoDim.push([arrayCombo[i1],arrayCombo[i2],arrayCombo[i3]])
            // console.log(arrayCombo[i1]+"  "+arrayCombo[i2]+"  "+arrayCombo[i3])
        }}}

        return innerArrayTwoDim;
    }


}
//############################################################################
//############################################################################
//############################################################################
//############################################################################
let task439 = new  Task439();
task439.display()