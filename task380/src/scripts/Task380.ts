//-------------------------------------
// |  1234  |    1234   |    1234   |
//-------------------------------------
// |  1234  |    1234   |    1234   |
//-------------------------------------
// |  1234  |    1234   |    1234   |
//-------------------------------------


class Task380{
//----------------------------------------------------------------
    public display(): void{
        const innerArray = this.arrayFun();
        // console.log(innerArray)
        // '1234', '1243', '1324',
        // '1342', '1423', '1432',

        const twoDimArrayFun = this.twoDimArrayFun(innerArray);
        // twoDimArrayFun.forEach((item, index)=>console.log(index, " ", item))

        // [ 1234, 1243, 4321, 6798 ],
        // [ 1234, 1324, 1243, 3801 ],
        const sortedArray = this.sortedArray(twoDimArrayFun);
        // sortedArray.forEach((item,index)=> console.log(index," ",item))

        const combinatorArray: number[][] = [];

        let isFlag = true;
        if(1)
            z1:for(let i1 = 0; i1 < sortedArray.length - 1; i1++){
                if(isFlag){
                    combinatorArray.push(sortedArray[i1])
                }
                // console.log(sortedArray[i1][3] ,"\t\t", sortedArray[i1+1][3])

                if(sortedArray[i1][3] === sortedArray[i1 + 1][3]){
                    isFlag = false;
                    // console.log(sortedArray[i1][3] ,"\t\t", sortedArray[i1+1][3])
                    // console.log(i1+1 ,"\t\t", sortedArray.length-1)
                    // console.log("------------------------------")
                    combinatorArray.push(sortedArray[i1 + 1]);
                }

                if(sortedArray[i1][3] !== sortedArray[i1 + 1][3] || i1 + 1 === sortedArray.length - 1){
                    isFlag = true;

                    if(combinatorArray.length < 2){
                        combinatorArray.length = 0;
                    }else{
                        // console.log("------------------------------------")
                        // console.log(combinatorArray.length)
                        // console.log(combinatorArray)
                        const findingNumber: number = combinatorArray[0][3];
                        this.combinatorArray(combinatorArray, findingNumber);

                        combinatorArray.length = 0;
                        // break z1;
                    }
                }
            }
    }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    public combinatorArray(twoDimArrayFun: number[][], findingNumber: number): void{
        // let cycle:number = 0;
        const len = twoDimArrayFun.length;
        for(let i1 = 0; i1 < len; i1++){
        for(let i2 = 0; i2 < len; i2++){
            if(this.isSkipCycle(twoDimArrayFun, i1, i2)) continue;
        for(let i3 = 0; i3 < len; i3++){
            if(this.isSkipCycle(twoDimArrayFun, i1, i3)) continue;
            if(this.isSkipCycle(twoDimArrayFun, i2, i3)) continue;

            const num1: number = (twoDimArrayFun[i1][0] + twoDimArrayFun[i2][0] + twoDimArrayFun[i3][0]);
            const num2: number = (twoDimArrayFun[i1][1] + twoDimArrayFun[i2][1] + twoDimArrayFun[i3][1]);
            const num3: number = (twoDimArrayFun[i1][2] + twoDimArrayFun[i2][2] + twoDimArrayFun[i3][2]);

            // x0_y0    x1_y0    x2_y0
            // x0_y1    x1_y1    x2_y1
            // x0_y2    x1_y2    x2_y2
const x0_y0__x1_y1__x2_y2:number = (twoDimArrayFun[i1][0] + twoDimArrayFun[i2][1] + twoDimArrayFun[i3][2]);
const x0_y2__x1_y1__x2_y0:number = (twoDimArrayFun[i1][2] + twoDimArrayFun[i2][1] + twoDimArrayFun[i3][0]);

            if(num1 === findingNumber && num2 === findingNumber && num3 === findingNumber &&
            x0_y0__x1_y1__x2_y2 === findingNumber && x0_y2__x1_y1__x2_y0 === findingNumber){
                
                console.log(findingNumber);
                console.log("[ \t\t\t\t\t" + x0_y0__x1_y1__x2_y2 +" ]");
                console.log(twoDimArrayFun[i1]);
                console.log(twoDimArrayFun[i2]);
                console.log(twoDimArrayFun[i3]);
                console.log("[ " + num1 + ", " + num2 + ", " + num3 + "  " + x0_y2__x1_y1__x2_y0 +" ]");
                console.log("------------------------");
            }
        }}}

        // for(let i1 = 0; i1 < len; i1++){
        // for(let i2 = i1+1; i2 < len; i2++){
        //     if(this.isSkipCycle(twoDimArrayFun,i1,i2)) continue;
        // for(let i3 = i2+1; i3 < len; i3++){
        //     if(this.isSkipCycle(twoDimArrayFun,i1,i3)) continue;
        //     if(this.isSkipCycle(twoDimArrayFun,i2,i3)) continue;
        //
        //         console.log(findingNumber)
        //         console.log(twoDimArrayFun[i1])
        //         console.log(twoDimArrayFun[i2])
        //         console.log(twoDimArrayFun[i3])
        //         // console.log(twoDimArrayFun[i1][0]+" "+twoDimArrayFun[i1][1]+" "+twoDimArrayFun[i1][2]);
        //         // console.log(twoDimArrayFun[i2][0]+" "+twoDimArrayFun[i2][1]+" "+twoDimArrayFun[i2][2]);
        //         // console.log(twoDimArrayFun[i3][0]+" "+twoDimArrayFun[i3][1]+" "+twoDimArrayFun[i3][2]);
        //         console.log("----------------------")
        //     // return true;
        // }}}
        // return false;
    }

//------------------------------------------------------------------
    public isSkipCycle(twoDimArrayFun: number[][], index_1: number, index_2: number): boolean{
        if(
            twoDimArrayFun[index_1][0] === twoDimArrayFun[index_2][0] ||
            twoDimArrayFun[index_1][0] === twoDimArrayFun[index_2][1] ||
            twoDimArrayFun[index_1][0] === twoDimArrayFun[index_2][2] ||

            twoDimArrayFun[index_1][1] === twoDimArrayFun[index_2][0] ||
            twoDimArrayFun[index_1][1] === twoDimArrayFun[index_2][1] ||
            twoDimArrayFun[index_1][1] === twoDimArrayFun[index_2][2] ||

            twoDimArrayFun[index_1][2] === twoDimArrayFun[index_2][0] ||
            twoDimArrayFun[index_1][2] === twoDimArrayFun[index_2][1] ||
            twoDimArrayFun[index_1][2] === twoDimArrayFun[index_2][2]){
            return true;
        }else{
            return false;
        }
    }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    public sortedArray(twoDimArrayFun: number[][]): number[][]{
        const twoDimArray = twoDimArrayFun;
        twoDimArray.sort((function(index){
            return function(a:any, b:any){
                return (a[index] === b[index] ? 0 :
                    (a[index] < b[index] ? -1 : 1));
            };
        })(3));
        return twoDimArray;
    }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    public twoDimArrayFun(innerArray: string[]): number[][]{
        // [ 1234, 1243, 1324 ],
        // [ 1234, 1243, 1324 ],
        // [ 1234, 1243, 1432 ],
        const twoDimArray: number[][] = [];
        // console.log(innerArray)
        const len = innerArray.length;

        for(let i1 = 0; i1 < len; i1++){
            for(let i2 = 0; i2 < len; i2++){
                if(innerArray[i1] === innerArray[i2]) continue
                for(let i3 = 0; i3 < len; i3++){
                    if(innerArray[i1] === innerArray[i3] ||
                        innerArray[i2] === innerArray[i3]) continue
                    twoDimArray.push([
                        Number(innerArray[i1]),
                        Number(innerArray[i2]),
                        Number(innerArray[i3]),
                        Number(innerArray[i1]) + Number(innerArray[i2]) + Number(innerArray[i3])
                    ])
                    // console.log(innerArray[i1]+" "+innerArray[i2]+" "+innerArray[i3])
                }
            }
        }
        return twoDimArray;
    }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    public arrayFun(): string[]{
        const innerArray: string[] = [];
        for(let i1 = 1; i1 <= 4; i1++){
            for(let i2 = 1; i2 <= 4; i2++){
                // if( i1 === i2)continue
                for(let i3 = 1; i3 <= 4; i3++){
                    // if( i1 === i3 || i2 === i3)continue
                    for(let i4 = 1; i4 <= 4; i4++){
                        // if( i1 === i4 || i2 === i4 || i3 === i4)continue

                        innerArray.push("" + i1 + "" + i2 + "" + i3 + "" + i4)
                        // console.log(i1," ",i2," ",i3," ",i4)
                    }
                }
            }
        }
        return innerArray;
    }

    // private addNull(str:string):string{
    //     switch(str.length){
    //         case 1: return '00'+ str
    //         case 2: return '0'+ str
    //         default: return str
    //     }
    // }

    // const makeUniq = (arr) => {
    //     const uniqSet = new Set(arr);
    //     return [...uniqSet];
    // }
}

let task380 = new Task380()
task380.display()


/*public enumerationArray(oneDimArray:number[]):void{
        // const twoDimArray:number[]= [
        //         1234, 1423, 4312,
        //         1243, 2314, 3412,
        //         1324, 1432, 4213
        //     ];

        const len = oneDimArray.length

        const answer:number = 6969;
        if(1)
        z1:for(let i1 = 0; i1 < len; i1++){
        for(let i2 = 0; i2 < len; i2++){
            if( oneDimArray[i1] === oneDimArray[i2] )continue;

        for(let i3 = 0; i3 < len; i3++){
            if( oneDimArray[i1] === oneDimArray[i3] ||
                oneDimArray[i2] === oneDimArray[i3] )continue;

        for(let i4 = 0; i4 < len; i4++){
            if( oneDimArray[i1] === oneDimArray[i4] ||
                oneDimArray[i2] === oneDimArray[i4] ||
                oneDimArray[i3] === oneDimArray[i4] )continue;

        for(let i5 = 0; i5 < len; i5++){
            if( oneDimArray[i1] === oneDimArray[i5] ||
                oneDimArray[i2] === oneDimArray[i5] ||
                oneDimArray[i3] === oneDimArray[i5] ||
                oneDimArray[i4] === oneDimArray[i5] )continue;

        for(let i6 = 0; i6 < len; i6++){
            if( oneDimArray[i1] === oneDimArray[i6] ||
                oneDimArray[i2] === oneDimArray[i6] ||
                oneDimArray[i3] === oneDimArray[i6] ||
                oneDimArray[i4] === oneDimArray[i6] ||
                oneDimArray[i5] === oneDimArray[i6] )continue;

        for(let i7 = 0; i7 < len; i7++){
            if( oneDimArray[i1] === oneDimArray[i7] ||
                oneDimArray[i2] === oneDimArray[i7] ||
                oneDimArray[i3] === oneDimArray[i7] ||
                oneDimArray[i4] === oneDimArray[i7] ||
                oneDimArray[i5] === oneDimArray[i7] ||
                oneDimArray[i6] === oneDimArray[i7] )continue;

        for(let i8 = 0; i8 < len; i8++){
            if( oneDimArray[i1] === oneDimArray[i8] ||
                oneDimArray[i2] === oneDimArray[i8] ||
                oneDimArray[i3] === oneDimArray[i8] ||
                oneDimArray[i4] === oneDimArray[i8] ||
                oneDimArray[i5] === oneDimArray[i8] ||
                oneDimArray[i6] === oneDimArray[i8] ||
                oneDimArray[i7] === oneDimArray[i8] )continue;

        for(let i9 = 0; i9 < len; i9++){
            if( oneDimArray[i1] === oneDimArray[i9] ||
                oneDimArray[i2] === oneDimArray[i9] ||
                oneDimArray[i3] === oneDimArray[i9] ||
                oneDimArray[i4] === oneDimArray[i9] ||
                oneDimArray[i5] === oneDimArray[i9] ||
                oneDimArray[i6] === oneDimArray[i9] ||
                oneDimArray[i7] === oneDimArray[i9] ||
                oneDimArray[i8] === oneDimArray[i9] )continue;


                if(
                    oneDimArray[i1] + oneDimArray[i4] + oneDimArray[i7] === answer &&
                    oneDimArray[i2] + oneDimArray[i5] + oneDimArray[i8] === answer &&
                    oneDimArray[i3] + oneDimArray[i6] + oneDimArray[i9] === answer &&

                    // twoDimArray[i1] + twoDimArray[i5] + twoDimArray[i9] === answer &&
                    // twoDimArray[i7] + twoDimArray[i5] + twoDimArray[i3] === answer &&

                    oneDimArray[i1] + oneDimArray[i2] + oneDimArray[i3] === answer &&
                    oneDimArray[i4] + oneDimArray[i5] + oneDimArray[i6] === answer &&
                    oneDimArray[i7] + oneDimArray[i8] + oneDimArray[i9] === answer ){

                    console.log(oneDimArray[i1] + " " + oneDimArray[i2] + " " + oneDimArray[i3])
                    console.log(oneDimArray[i4] + " " + oneDimArray[i5] + " " + oneDimArray[i6])
                    console.log(oneDimArray[i7] + " " + oneDimArray[i8] + " " + oneDimArray[i9])
                    console.log("-------------------------------------------")
                    break z1;
                }

        }}}}}}}}}
    }*/